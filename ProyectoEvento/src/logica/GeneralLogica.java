
package logica;

import java.util.List;
import modelo.General;
import persistencia.GeneralJpaController;


public class GeneralLogica {
    
    private GeneralJpaController generalDAO = new GeneralJpaController();
    
    public List<General> consultarTodos() throws Exception{
        return generalDAO.findGeneralEntities();
    }
    
    public void registrarGeneral (General genID) throws Exception{
        generalDAO.create(genID);
    }
    
    public void modificarGeneral (General genID) throws Exception{
        generalDAO.edit(genID);
    }
    
    public void eliminarGeneral (String genID) throws Exception{
        generalDAO.destroy(genID);
    }
    
    public List<General> buscarCategoria (String categoria) throws Exception{
        return generalDAO.buscarCateg(categoria);
    }
}
