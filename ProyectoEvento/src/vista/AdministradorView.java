
package vista;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import logica.GeneralLogica;
import modelo.Conferencista;
import modelo.General;


public class AdministradorView extends javax.swing.JFrame {
    
    General modeloGen = new General();
    GeneralLogica logicaGen = new GeneralLogica(); 
    
    public static DefaultTableModel datoTabla;
    
    public AdministradorView() {
        initComponents();
        setLocationRelativeTo(null);
        //crearModeloTabla();
        //mostrarDatosTabla();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        confeTab = new javax.swing.JTabbedPane();
        genContenido = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        genBtnRegistrar = new javax.swing.JButton();
        genBtnModificar = new javax.swing.JButton();
        genBtnBorrar = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        listCategoria = new javax.swing.JComboBox();
        nombreGen = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        genTabla = new javax.swing.JTable();
        buscarListCategoria = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        confeBtnRegistrar = new javax.swing.JButton();
        confeBtnModificar = new javax.swing.JButton();
        confeBtnBorrar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        confeTab.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                confeTabMouseClicked(evt);
            }
        });

        genContenido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                genContenidoMouseClicked(evt);
            }
        });
        genContenido.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Operaciones"));

        genBtnRegistrar.setText("Registrar");
        genBtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genBtnRegistrarActionPerformed(evt);
            }
        });

        genBtnModificar.setText("Modificar");
        genBtnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genBtnModificarActionPerformed(evt);
            }
        });

        genBtnBorrar.setText("Borrar");
        genBtnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                genBtnBorrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(genBtnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(genBtnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(genBtnBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(genBtnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(genBtnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(genBtnBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        genContenido.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 285, -1, -1));

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Formulario Datos Generales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        listCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tipo Usuario", "Tipo Evento", "Dependencia Organizacional" }));
        listCategoria.setPreferredSize(new java.awt.Dimension(160, 25));

        nombreGen.setPreferredSize(new java.awt.Dimension(59, 25));

        jLabel1.setText("Categoría:");

        jLabel2.setText("Nombre:");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nombreGen, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(listCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(28, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(listCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(27, 27, 27)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombreGen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        genContenido.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 69, -1, -1));

        genTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        genTabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                genTablaMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(genTabla);

        genContenido.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 60, 360, 310));

        buscarListCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-", "Tipo Usuario", "Tipo Evento", "Dependencia Organizacional" }));
        buscarListCategoria.setPreferredSize(new java.awt.Dimension(160, 25));
        buscarListCategoria.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buscarListCategoriaActionPerformed(evt);
            }
        });
        genContenido.add(buscarListCategoria, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 20, -1, -1));

        confeTab.addTab("Datos Generales", genContenido);

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Formulario Conferencista", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 327, Short.MAX_VALUE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 177, Short.MAX_VALUE)
        );

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(46, 35, -1, -1));

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Operaciones"));

        confeBtnRegistrar.setText("Registrar");
        confeBtnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confeBtnRegistrarActionPerformed(evt);
            }
        });

        confeBtnModificar.setText("Modificar");
        confeBtnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confeBtnModificarActionPerformed(evt);
            }
        });

        confeBtnBorrar.setText("Borrar");
        confeBtnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confeBtnBorrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(confeBtnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(confeBtnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(confeBtnBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(confeBtnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(confeBtnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(confeBtnBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 285, -1, -1));

        confeTab.addTab("Conferencista", jPanel2);

        getContentPane().add(confeTab, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 75, 850, 470));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void genBtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genBtnRegistrarActionPerformed
        try {
            List listGenID = logicaGen.consultarTodos();
            
            if(!listCategoria.getSelectedItem().equals("-")
                    && !nombreGen.getText().isEmpty()){
                
                if(listCategoria.getSelectedItem().equals("Tipo Usuario")){                         
                    String genId = "TPUSU"+(listGenID.size()+1);                   
                    modeloGen.setGenId(genId);
                    modeloGen.setGenNombre(nombreGen.getText());
                    modeloGen.setGenCategoria((String) listCategoria.getSelectedItem());
                    logicaGen.registrarGeneral(modeloGen);
                    JOptionPane.showMessageDialog(null, "¡Registro Exitoso!");
                    genLimpiarCampo();
                    genModeloTabla(); // Diseño de la Tabla
                    genDatoTabla(); // Muestra los datos en la Tabla
                }
                else 
                    if(listCategoria.getSelectedItem().equals("Tipo Evento")){
                        String genId = "TPEVE"+(listGenID.size()+1);                   
                        modeloGen.setGenId(genId);
                        modeloGen.setGenNombre(nombreGen.getText());
                        modeloGen.setGenCategoria((String) listCategoria.getSelectedItem());
                        logicaGen.registrarGeneral(modeloGen);
                        JOptionPane.showMessageDialog(null, "¡Registro Exitoso!");
                        genLimpiarCampo();
                        genModeloTabla(); // Diseño de la Tabla
                        genDatoTabla(); // Muestra los datos en la Tabla
                    }
                    else 
                        if(listCategoria.getSelectedItem().equals("Dependencia Organizacional")){
                            String genId = "DPORG"+(listGenID.size()+1);                   
                            modeloGen.setGenId(genId);
                            modeloGen.setGenNombre(nombreGen.getText()); 
                            modeloGen.setGenCategoria((String) listCategoria.getSelectedItem());
                            logicaGen.registrarGeneral(modeloGen);
                            JOptionPane.showMessageDialog(null, "¡Registro Exitoso!");
                            genLimpiarCampo();
                            genModeloTabla(); // Diseño de la Tabla
                            genDatoTabla(); // Muestra los datos en la Tabla
                    }
            }else{
                JOptionPane.showMessageDialog(null, "¡AVISO!\nPor favor, revise que no haya campos vacíos");
            }
        } catch (Exception ex) {
            Logger.getLogger(AdministradorView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_genBtnRegistrarActionPerformed

    private void genBtnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genBtnModificarActionPerformed
        int fila = genTabla.getSelectedRow();

        if(fila >= 0){
            if(!listCategoria.getSelectedItem().equals("-")
                && !nombreGen.getText().isEmpty())
            {
                // Si todos los campos tienen los mismos datos
                // que en la BD arrojará un mensaje
                if(datoTabla.getValueAt(fila, 2).equals(listCategoria.getSelectedItem())
                    && datoTabla.getValueAt(fila, 1).equals(nombreGen.getText()) )
                {
                    JOptionPane.showMessageDialog(null, "¡AVISO!\nNo se ha modificado nada");
                }else{
                    try {
                        String genID = (String) datoTabla.getValueAt(fila, 0);
                        modeloGen.setGenId(genID);
                        modeloGen.setGenNombre(nombreGen.getText());
                        modeloGen.setGenCategoria((String) listCategoria.getSelectedItem());  
                        logicaGen.modificarGeneral(modeloGen);
                        JOptionPane.showMessageDialog(null, "¡Modificación Exitosa!");
                        genLimpiarCampo();
                        LimpiarTabla(genTabla);
                        genModeloTabla(); // Diseño de la Tabla
                        genDatoTabla(); // Muestra los datos de la Tabla
                        genBtnRegistrar.setEnabled(true);
                    } catch (Exception ex) {
                        Logger.getLogger(AdministradorView.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }else{
                JOptionPane.showMessageDialog(null, "¡AVISO!\nHay campos vacíos, revise por favor");
            }
        }else{
            JOptionPane.showMessageDialog(null, "Fila no seleccionada");
        }
    }//GEN-LAST:event_genBtnModificarActionPerformed

    private void genBtnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_genBtnBorrarActionPerformed
        int fila = genTabla.getSelectedRow();
        if (fila >= 0) {
            String genID = (String) datoTabla.getValueAt(fila, 0);
            int resp = JOptionPane.showConfirmDialog(null, "¿Está seguro que quiere Borrar el Registro?", "Mensaje", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (JOptionPane.YES_NO_OPTION == resp) {
                try {
                    logicaGen.eliminarGeneral(genID);
                    LimpiarTabla(genTabla);
                    genLimpiarCampo();
                    genModeloTabla(); // Diseño de la Tabla
                    genDatoTabla(); // Muestra los datos en la Tabla
                } catch (Exception ex) {
                    Logger.getLogger(AdministradorView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }else{
            JOptionPane.showMessageDialog(null, "Fila no seleccionada");
        }
    }//GEN-LAST:event_genBtnBorrarActionPerformed

    private void genTablaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_genTablaMousePressed
        genBtnRegistrar.setEnabled(false);
        int fila = genTabla.getSelectedRow();

        if (fila >= 0) {
            listCategoria.setSelectedItem((String) datoTabla.getValueAt(fila, 2));
            nombreGen.setText((String) datoTabla.getValueAt(fila, 1));
        } else {
            JOptionPane.showMessageDialog(null, "Fila no seleccionada.");
        }
    }//GEN-LAST:event_genTablaMousePressed

    private void confeTabMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_confeTabMouseClicked
        LimpiarTabla(genTabla);
        genModeloTabla(); // Diseño de la Tabla
        genDatoTabla(); // Muestra los datos en la Tabla
    }//GEN-LAST:event_confeTabMouseClicked

    private void genContenidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_genContenidoMouseClicked
        genBtnRegistrar.setEnabled(true);
        genLimpiarCampo();
        genModeloTabla(); // Diseño de la Tabla
        genDatoTabla(); // Mostrar los datos en la Tabla
    }//GEN-LAST:event_genContenidoMouseClicked

    private void buscarListCategoriaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buscarListCategoriaActionPerformed
        LimpiarTabla(genTabla);
        
        if(!buscarListCategoria.getSelectedItem().equals("-")){
            //JOptionPane.showMessageDialog(null, "Entroo");
            try {
                genModeloTabla();
                String dato = buscarListCategoria.getSelectedItem().toString();
                
                Object A[] = null;
                List<General> lista =  logicaGen.buscarCategoria(dato);
                
                for (int i = 0; i < lista.size(); i++) {
                    datoTabla.addRow(A);
                    datoTabla.setValueAt(lista.get(i).getGenId(), i, 0);
                    datoTabla.setValueAt(lista.get(i).getGenNombre(), i, 1);
                    datoTabla.setValueAt(lista.get(i).getGenCategoria(), i, 2);
                }
                genTabla.setModel(datoTabla);
            } catch (Exception ex) {
                Logger.getLogger(AdministradorView.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_buscarListCategoriaActionPerformed

    private void confeBtnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confeBtnRegistrarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_confeBtnRegistrarActionPerformed

    private void confeBtnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confeBtnModificarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_confeBtnModificarActionPerformed

    private void confeBtnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confeBtnBorrarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_confeBtnBorrarActionPerformed
    
    /***********************************************FUNCIONES PERSONALIZADAS************************************/
    private void genModeloTabla() {
        datoTabla = (new DefaultTableModel(
                         null, new String[]{
                             "Código", "Nombre", "Categoría" //Se coloca el Nombre de las columnas Compuesta por la TABLA DE BD
                         }
                    )
        {
            //Indica que no sea EDITABLE LA INFORMACIÓN que hay en la tabla
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {  
                return false;
            }                
        });
    }
    
    private void genDatoTabla(){   
        try {  
            //OBJECT A[] -> Nos permite entrar a la función addRow[] para añadir
            //la cantidad de filas como hay de información en la Base de Datos, es decir,
            //añadir registros a la tabla
            Object A[] = null;
            List<General> genListDato = logicaGen.consultarTodos();
            JOptionPane.showMessageDialog(null, genListDato.size());
            for (int i = 0; i < genListDato.size(); i++) {
                datoTabla.addRow(A);
                datoTabla.setValueAt(genListDato.get(i).getGenId(), i, 0);
                datoTabla.setValueAt(genListDato.get(i).getGenNombre(), i, 1); 
                datoTabla.setValueAt(genListDato.get(i).getGenCategoria(), i, 2);         
            }
            genTabla.setModel(datoTabla);
        } catch (Exception ex) {
            Logger.getLogger(AdministradorView.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    private void genLimpiarCampo() {
        listCategoria.setSelectedItem("-");
        nombreGen.setText("");
    }
    
    //Sirve para Actualizar cada acción que se va a realizar en una Consulta
    //como la de Eliminar Y Modificar
    public void LimpiarTabla(JTable tabla) {
        for (int i = 0; i < tabla.getRowCount(); i++) {
            datoTabla.removeRow(i);
            i -= 1;
        }
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(AdministradorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(AdministradorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(AdministradorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(AdministradorView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new AdministradorView().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox buscarListCategoria;
    private javax.swing.JButton confeBtnBorrar;
    private javax.swing.JButton confeBtnModificar;
    private javax.swing.JButton confeBtnRegistrar;
    private javax.swing.JTabbedPane confeTab;
    private javax.swing.JButton genBtnBorrar;
    private javax.swing.JButton genBtnModificar;
    private javax.swing.JButton genBtnRegistrar;
    private javax.swing.JPanel genContenido;
    private javax.swing.JTable genTabla;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox listCategoria;
    private javax.swing.JTextField nombreGen;
    // End of variables declaration//GEN-END:variables
}
