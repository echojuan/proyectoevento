/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "evento" , schema = "eventos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Evento.findAll", query = "SELECT e FROM Evento e"),
    @NamedQuery(name = "Evento.findByEvId", query = "SELECT e FROM Evento e WHERE e.evId = :evId"),
    @NamedQuery(name = "Evento.findByEvNombre", query = "SELECT e FROM Evento e WHERE e.evNombre = :evNombre"),
    @NamedQuery(name = "Evento.findByEvFecInicio", query = "SELECT e FROM Evento e WHERE e.evFecInicio = :evFecInicio"),
    @NamedQuery(name = "Evento.findByEvFecFin", query = "SELECT e FROM Evento e WHERE e.evFecFin = :evFecFin"),
    @NamedQuery(name = "Evento.findByEvCupo", query = "SELECT e FROM Evento e WHERE e.evCupo = :evCupo"),
    @NamedQuery(name = "Evento.findByEvDetalle", query = "SELECT e FROM Evento e WHERE e.evDetalle = :evDetalle"),
    @NamedQuery(name = "Evento.findByEvEstado", query = "SELECT e FROM Evento e WHERE e.evEstado = :evEstado")})
public class Evento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ev_id")
    private Integer evId;
    @Basic(optional = false)
    @Column(name = "ev_nombre")
    private String evNombre;
    @Basic(optional = false)
    @Column(name = "ev_fec_inicio")
    @Temporal(TemporalType.DATE)
    private Date evFecInicio;
    @Basic(optional = false)
    @Column(name = "ev_fec_fin")
    @Temporal(TemporalType.DATE)
    private Date evFecFin;
    @Basic(optional = false)
    @Column(name = "ev_cupo")
    private short evCupo;
    @Column(name = "ev_detalle")
    private String evDetalle;
    @Basic(optional = false)
    @Column(name = "ev_estado")
    private String evEstado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evImpleEvId")
    private List<AsignaImple> asignaImpleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evEspEvId")
    private List<AsignaEspacio> asignaEspacioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "encEvId")
    private List<Encuesta> encuestaList;
    @JoinColumn(name = "ev_cod_eve", referencedColumnName = "gen_id")
    @ManyToOne(optional = false)
    private General evCodEve;
    @JoinColumn(name = "ev_cod_depe", referencedColumnName = "gen_id")
    @ManyToOne(optional = false)
    private General evCodDepe;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evento")
    private List<Conferencia> conferenciaList;

    public Evento() {
    }

    public Evento(Integer evId) {
        this.evId = evId;
    }

    public Evento(Integer evId, String evNombre, Date evFecInicio, Date evFecFin, short evCupo, String evEstado) {
        this.evId = evId;
        this.evNombre = evNombre;
        this.evFecInicio = evFecInicio;
        this.evFecFin = evFecFin;
        this.evCupo = evCupo;
        this.evEstado = evEstado;
    }

    public Integer getEvId() {
        return evId;
    }

    public void setEvId(Integer evId) {
        this.evId = evId;
    }

    public String getEvNombre() {
        return evNombre;
    }

    public void setEvNombre(String evNombre) {
        this.evNombre = evNombre;
    }

    public Date getEvFecInicio() {
        return evFecInicio;
    }

    public void setEvFecInicio(Date evFecInicio) {
        this.evFecInicio = evFecInicio;
    }

    public Date getEvFecFin() {
        return evFecFin;
    }

    public void setEvFecFin(Date evFecFin) {
        this.evFecFin = evFecFin;
    }

    public short getEvCupo() {
        return evCupo;
    }

    public void setEvCupo(short evCupo) {
        this.evCupo = evCupo;
    }

    public String getEvDetalle() {
        return evDetalle;
    }

    public void setEvDetalle(String evDetalle) {
        this.evDetalle = evDetalle;
    }

    public String getEvEstado() {
        return evEstado;
    }

    public void setEvEstado(String evEstado) {
        this.evEstado = evEstado;
    }

    @XmlTransient
    public List<AsignaImple> getAsignaImpleList() {
        return asignaImpleList;
    }

    public void setAsignaImpleList(List<AsignaImple> asignaImpleList) {
        this.asignaImpleList = asignaImpleList;
    }

    @XmlTransient
    public List<AsignaEspacio> getAsignaEspacioList() {
        return asignaEspacioList;
    }

    public void setAsignaEspacioList(List<AsignaEspacio> asignaEspacioList) {
        this.asignaEspacioList = asignaEspacioList;
    }

    @XmlTransient
    public List<Encuesta> getEncuestaList() {
        return encuestaList;
    }

    public void setEncuestaList(List<Encuesta> encuestaList) {
        this.encuestaList = encuestaList;
    }

    public General getEvCodEve() {
        return evCodEve;
    }

    public void setEvCodEve(General evCodEve) {
        this.evCodEve = evCodEve;
    }

    public General getEvCodDepe() {
        return evCodDepe;
    }

    public void setEvCodDepe(General evCodDepe) {
        this.evCodDepe = evCodDepe;
    }

    @XmlTransient
    public List<Conferencia> getConferenciaList() {
        return conferenciaList;
    }

    public void setConferenciaList(List<Conferencia> conferenciaList) {
        this.conferenciaList = conferenciaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evId != null ? evId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Evento)) {
            return false;
        }
        Evento other = (Evento) object;
        if ((this.evId == null && other.evId != null) || (this.evId != null && !this.evId.equals(other.evId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Evento[ evId=" + evId + " ]";
    }
    
}
