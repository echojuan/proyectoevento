/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "conferencista")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conferencista.findAll", query = "SELECT c FROM Conferencista c"),
    @NamedQuery(name = "Conferencista.findByConCedula", query = "SELECT c FROM Conferencista c WHERE c.conCedula = :conCedula"),
    @NamedQuery(name = "Conferencista.findByConNombre", query = "SELECT c FROM Conferencista c WHERE c.conNombre = :conNombre"),
    @NamedQuery(name = "Conferencista.findByConApel", query = "SELECT c FROM Conferencista c WHERE c.conApel = :conApel"),
    @NamedQuery(name = "Conferencista.findByConEspecial", query = "SELECT c FROM Conferencista c WHERE c.conEspecial = :conEspecial"),
    @NamedQuery(name = "Conferencista.findByConTelefo", query = "SELECT c FROM Conferencista c WHERE c.conTelefo = :conTelefo"),
    @NamedQuery(name = "Conferencista.findByConCorreo", query = "SELECT c FROM Conferencista c WHERE c.conCorreo = :conCorreo"),
    @NamedQuery(name = "Conferencista.findByConDpto", query = "SELECT c FROM Conferencista c WHERE c.conDpto = :conDpto"),
    @NamedQuery(name = "Conferencista.findByConDispo", query = "SELECT c FROM Conferencista c WHERE c.conDispo = :conDispo"),
    @NamedQuery(name = "Conferencista.findByConferenciaConferId", query = "SELECT c FROM Conferencista c WHERE c.conferenciaConferId = :conferenciaConferId")})
public class Conferencista implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "con_cedula")
    private String conCedula;
    @Basic(optional = false)
    @Column(name = "con_nombre")
    private String conNombre;
    @Basic(optional = false)
    @Column(name = "con_apel")
    private String conApel;
    @Basic(optional = false)
    @Column(name = "con_especial")
    private String conEspecial;
    @Basic(optional = false)
    @Column(name = "con_telefo")
    private String conTelefo;
    @Basic(optional = false)
    @Column(name = "con_correo")
    private String conCorreo;
    @Column(name = "con_dpto")
    private String conDpto;
    @Basic(optional = false)
    @Column(name = "con_dispo")
    private String conDispo;
    @Basic(optional = false)
    @Column(name = "conferencia_confer_id")
    private int conferenciaConferId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conferencista")
    private List<Conferencia> conferenciaList;

    public Conferencista() {
    }

    public Conferencista(String conCedula) {
        this.conCedula = conCedula;
    }

    public Conferencista(String conCedula, String conNombre, String conApel, String conEspecial, String conTelefo, String conCorreo, String conDispo, int conferenciaConferId) {
        this.conCedula = conCedula;
        this.conNombre = conNombre;
        this.conApel = conApel;
        this.conEspecial = conEspecial;
        this.conTelefo = conTelefo;
        this.conCorreo = conCorreo;
        this.conDispo = conDispo;
        this.conferenciaConferId = conferenciaConferId;
    }

    public String getConCedula() {
        return conCedula;
    }

    public void setConCedula(String conCedula) {
        this.conCedula = conCedula;
    }

    public String getConNombre() {
        return conNombre;
    }

    public void setConNombre(String conNombre) {
        this.conNombre = conNombre;
    }

    public String getConApel() {
        return conApel;
    }

    public void setConApel(String conApel) {
        this.conApel = conApel;
    }

    public String getConEspecial() {
        return conEspecial;
    }

    public void setConEspecial(String conEspecial) {
        this.conEspecial = conEspecial;
    }

    public String getConTelefo() {
        return conTelefo;
    }

    public void setConTelefo(String conTelefo) {
        this.conTelefo = conTelefo;
    }

    public String getConCorreo() {
        return conCorreo;
    }

    public void setConCorreo(String conCorreo) {
        this.conCorreo = conCorreo;
    }

    public String getConDpto() {
        return conDpto;
    }

    public void setConDpto(String conDpto) {
        this.conDpto = conDpto;
    }

    public String getConDispo() {
        return conDispo;
    }

    public void setConDispo(String conDispo) {
        this.conDispo = conDispo;
    }

    public int getConferenciaConferId() {
        return conferenciaConferId;
    }

    public void setConferenciaConferId(int conferenciaConferId) {
        this.conferenciaConferId = conferenciaConferId;
    }

    @XmlTransient
    public List<Conferencia> getConferenciaList() {
        return conferenciaList;
    }

    public void setConferenciaList(List<Conferencia> conferenciaList) {
        this.conferenciaList = conferenciaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conCedula != null ? conCedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conferencista)) {
            return false;
        }
        Conferencista other = (Conferencista) object;
        if ((this.conCedula == null && other.conCedula != null) || (this.conCedula != null && !this.conCedula.equals(other.conCedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Conferencista[ conCedula=" + conCedula + " ]";
    }
    
}
