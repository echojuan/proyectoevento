/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "asigna_imple")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AsignaImple.findAll", query = "SELECT a FROM AsignaImple a"),
    @NamedQuery(name = "AsignaImple.findByEvImpleId", query = "SELECT a FROM AsignaImple a WHERE a.evImpleId = :evImpleId"),
    @NamedQuery(name = "AsignaImple.findByEvImpleFecAsigna", query = "SELECT a FROM AsignaImple a WHERE a.evImpleFecAsigna = :evImpleFecAsigna")})
public class AsignaImple implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ev_imple_id")
    private Integer evImpleId;
    @Basic(optional = false)
    @Column(name = "ev_imple_fec_asigna")
    @Temporal(TemporalType.DATE)
    private Date evImpleFecAsigna;
    @JoinColumn(name = "ev_imple_ev_id", referencedColumnName = "ev_id")
    @ManyToOne(optional = false)
    private Evento evImpleEvId;
    @JoinColumn(name = "ev_imple_imp_id", referencedColumnName = "imp_id")
    @ManyToOne(optional = false)
    private Implementos evImpleImpId;
    @JoinColumn(name = "ev_imple_admin", referencedColumnName = "usu_cedula")
    @ManyToOne(optional = false)
    private Usuario evImpleAdmin;

    public AsignaImple() {
    }

    public AsignaImple(Integer evImpleId) {
        this.evImpleId = evImpleId;
    }

    public AsignaImple(Integer evImpleId, Date evImpleFecAsigna) {
        this.evImpleId = evImpleId;
        this.evImpleFecAsigna = evImpleFecAsigna;
    }

    public Integer getEvImpleId() {
        return evImpleId;
    }

    public void setEvImpleId(Integer evImpleId) {
        this.evImpleId = evImpleId;
    }

    public Date getEvImpleFecAsigna() {
        return evImpleFecAsigna;
    }

    public void setEvImpleFecAsigna(Date evImpleFecAsigna) {
        this.evImpleFecAsigna = evImpleFecAsigna;
    }

    public Evento getEvImpleEvId() {
        return evImpleEvId;
    }

    public void setEvImpleEvId(Evento evImpleEvId) {
        this.evImpleEvId = evImpleEvId;
    }

    public Implementos getEvImpleImpId() {
        return evImpleImpId;
    }

    public void setEvImpleImpId(Implementos evImpleImpId) {
        this.evImpleImpId = evImpleImpId;
    }

    public Usuario getEvImpleAdmin() {
        return evImpleAdmin;
    }

    public void setEvImpleAdmin(Usuario evImpleAdmin) {
        this.evImpleAdmin = evImpleAdmin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evImpleId != null ? evImpleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsignaImple)) {
            return false;
        }
        AsignaImple other = (AsignaImple) object;
        if ((this.evImpleId == null && other.evImpleId != null) || (this.evImpleId != null && !this.evImpleId.equals(other.evImpleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.AsignaImple[ evImpleId=" + evImpleId + " ]";
    }
    
}
