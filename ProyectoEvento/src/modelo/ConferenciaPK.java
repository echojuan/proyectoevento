/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author Usuario
 */
@Embeddable
public class ConferenciaPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "conf_id")
    private int confId;
    @Basic(optional = false)
    @Column(name = "conf_ev_id")
    private int confEvId;
    @Basic(optional = false)
    @Column(name = "conf_cedu_confere")
    private String confCeduConfere;

    public ConferenciaPK() {
    }

    public ConferenciaPK(int confId, int confEvId, String confCeduConfere) {
        this.confId = confId;
        this.confEvId = confEvId;
        this.confCeduConfere = confCeduConfere;
    }

    public int getConfId() {
        return confId;
    }

    public void setConfId(int confId) {
        this.confId = confId;
    }

    public int getConfEvId() {
        return confEvId;
    }

    public void setConfEvId(int confEvId) {
        this.confEvId = confEvId;
    }

    public String getConfCeduConfere() {
        return confCeduConfere;
    }

    public void setConfCeduConfere(String confCeduConfere) {
        this.confCeduConfere = confCeduConfere;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) confId;
        hash += (int) confEvId;
        hash += (confCeduConfere != null ? confCeduConfere.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConferenciaPK)) {
            return false;
        }
        ConferenciaPK other = (ConferenciaPK) object;
        if (this.confId != other.confId) {
            return false;
        }
        if (this.confEvId != other.confEvId) {
            return false;
        }
        if ((this.confCeduConfere == null && other.confCeduConfere != null) || (this.confCeduConfere != null && !this.confCeduConfere.equals(other.confCeduConfere))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ConferenciaPK[ confId=" + confId + ", confEvId=" + confEvId + ", confCeduConfere=" + confCeduConfere + " ]";
    }
    
}
