/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "asigna_espacio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AsignaEspacio.findAll", query = "SELECT a FROM AsignaEspacio a"),
    @NamedQuery(name = "AsignaEspacio.findByEvEspId", query = "SELECT a FROM AsignaEspacio a WHERE a.evEspId = :evEspId"),
    @NamedQuery(name = "AsignaEspacio.findByEvEspFecAsigna", query = "SELECT a FROM AsignaEspacio a WHERE a.evEspFecAsigna = :evEspFecAsigna")})
public class AsignaEspacio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ev_esp_id")
    private Integer evEspId;
    @Basic(optional = false)
    @Column(name = "ev_esp_fec_asigna")
    @Temporal(TemporalType.DATE)
    private Date evEspFecAsigna;
    @JoinColumn(name = "ev_esp_esp_id", referencedColumnName = "esp_id")
    @ManyToOne(optional = false)
    private Espacios evEspEspId;
    @JoinColumn(name = "ev_esp_ev_id", referencedColumnName = "ev_id")
    @ManyToOne(optional = false)
    private Evento evEspEvId;
    @JoinColumn(name = "ev_esp_admin", referencedColumnName = "usu_cedula")
    @ManyToOne(optional = false)
    private Usuario evEspAdmin;

    public AsignaEspacio() {
    }

    public AsignaEspacio(Integer evEspId) {
        this.evEspId = evEspId;
    }

    public AsignaEspacio(Integer evEspId, Date evEspFecAsigna) {
        this.evEspId = evEspId;
        this.evEspFecAsigna = evEspFecAsigna;
    }

    public Integer getEvEspId() {
        return evEspId;
    }

    public void setEvEspId(Integer evEspId) {
        this.evEspId = evEspId;
    }

    public Date getEvEspFecAsigna() {
        return evEspFecAsigna;
    }

    public void setEvEspFecAsigna(Date evEspFecAsigna) {
        this.evEspFecAsigna = evEspFecAsigna;
    }

    public Espacios getEvEspEspId() {
        return evEspEspId;
    }

    public void setEvEspEspId(Espacios evEspEspId) {
        this.evEspEspId = evEspEspId;
    }

    public Evento getEvEspEvId() {
        return evEspEvId;
    }

    public void setEvEspEvId(Evento evEspEvId) {
        this.evEspEvId = evEspEvId;
    }

    public Usuario getEvEspAdmin() {
        return evEspAdmin;
    }

    public void setEvEspAdmin(Usuario evEspAdmin) {
        this.evEspAdmin = evEspAdmin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (evEspId != null ? evEspId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsignaEspacio)) {
            return false;
        }
        AsignaEspacio other = (AsignaEspacio) object;
        if ((this.evEspId == null && other.evEspId != null) || (this.evEspId != null && !this.evEspId.equals(other.evEspId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.AsignaEspacio[ evEspId=" + evEspId + " ]";
    }
    
}
