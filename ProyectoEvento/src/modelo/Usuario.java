/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "usuario" , schema = "eventos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u"),
    @NamedQuery(name = "Usuario.findByUsuCedula", query = "SELECT u FROM Usuario u WHERE u.usuCedula = :usuCedula"),
    @NamedQuery(name = "Usuario.findByUsuPass", query = "SELECT u FROM Usuario u WHERE u.usuPass = :usuPass"),
    @NamedQuery(name = "Usuario.findByUsuNom", query = "SELECT u FROM Usuario u WHERE u.usuNom = :usuNom"),
    @NamedQuery(name = "Usuario.findByUsuApel", query = "SELECT u FROM Usuario u WHERE u.usuApel = :usuApel"),
    @NamedQuery(name = "Usuario.findByUsuTel", query = "SELECT u FROM Usuario u WHERE u.usuTel = :usuTel"),
    @NamedQuery(name = "Usuario.findByUsuCorreo", query = "SELECT u FROM Usuario u WHERE u.usuCorreo = :usuCorreo"),
    @NamedQuery(name = "Usuario.findByUsuGenero", query = "SELECT u FROM Usuario u WHERE u.usuGenero = :usuGenero")})
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "usu_cedula")
    private String usuCedula;
    @Basic(optional = false)
    @Column(name = "usu_pass")
    private String usuPass;
    @Basic(optional = false)
    @Column(name = "usu_nom")
    private String usuNom;
    @Basic(optional = false)
    @Column(name = "usu_apel")
    private String usuApel;
    @Column(name = "usu_tel")
    private String usuTel;
    @Basic(optional = false)
    @Column(name = "usu_correo")
    private String usuCorreo;
    @Column(name = "usu_genero")
    private String usuGenero;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evImpleAdmin")
    private List<AsignaImple> asignaImpleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evEspAdmin")
    private List<AsignaEspacio> asignaEspacioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "encUsuCedula")
    private List<Encuesta> encuestaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evUsuUsuCedula")
    private List<Incripcion> incripcionList;
    @JoinColumn(name = "usu_tipo_gen_id", referencedColumnName = "gen_id")
    @ManyToOne(optional = false)
    private General usuTipoGenId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "impAlmacenista")
    private List<Implementos> implementosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "espAdmin")
    private List<Espacios> espaciosList;

    public Usuario() {
    }

    public Usuario(String usuCedula) {
        this.usuCedula = usuCedula;
    }

    public Usuario(String usuCedula, String usuPass, String usuNom, String usuApel, String usuCorreo) {
        this.usuCedula = usuCedula;
        this.usuPass = usuPass;
        this.usuNom = usuNom;
        this.usuApel = usuApel;
        this.usuCorreo = usuCorreo;
    }

    public String getUsuCedula() {
        return usuCedula;
    }

    public void setUsuCedula(String usuCedula) {
        this.usuCedula = usuCedula;
    }

    public String getUsuPass() {
        return usuPass;
    }

    public void setUsuPass(String usuPass) {
        this.usuPass = usuPass;
    }

    public String getUsuNom() {
        return usuNom;
    }

    public void setUsuNom(String usuNom) {
        this.usuNom = usuNom;
    }

    public String getUsuApel() {
        return usuApel;
    }

    public void setUsuApel(String usuApel) {
        this.usuApel = usuApel;
    }

    public String getUsuTel() {
        return usuTel;
    }

    public void setUsuTel(String usuTel) {
        this.usuTel = usuTel;
    }

    public String getUsuCorreo() {
        return usuCorreo;
    }

    public void setUsuCorreo(String usuCorreo) {
        this.usuCorreo = usuCorreo;
    }

    public String getUsuGenero() {
        return usuGenero;
    }

    public void setUsuGenero(String usuGenero) {
        this.usuGenero = usuGenero;
    }

    @XmlTransient
    public List<AsignaImple> getAsignaImpleList() {
        return asignaImpleList;
    }

    public void setAsignaImpleList(List<AsignaImple> asignaImpleList) {
        this.asignaImpleList = asignaImpleList;
    }

    @XmlTransient
    public List<AsignaEspacio> getAsignaEspacioList() {
        return asignaEspacioList;
    }

    public void setAsignaEspacioList(List<AsignaEspacio> asignaEspacioList) {
        this.asignaEspacioList = asignaEspacioList;
    }

    @XmlTransient
    public List<Encuesta> getEncuestaList() {
        return encuestaList;
    }

    public void setEncuestaList(List<Encuesta> encuestaList) {
        this.encuestaList = encuestaList;
    }

    @XmlTransient
    public List<Incripcion> getIncripcionList() {
        return incripcionList;
    }

    public void setIncripcionList(List<Incripcion> incripcionList) {
        this.incripcionList = incripcionList;
    }

    public General getUsuTipoGenId() {
        return usuTipoGenId;
    }

    public void setUsuTipoGenId(General usuTipoGenId) {
        this.usuTipoGenId = usuTipoGenId;
    }

    @XmlTransient
    public List<Implementos> getImplementosList() {
        return implementosList;
    }

    public void setImplementosList(List<Implementos> implementosList) {
        this.implementosList = implementosList;
    }

    @XmlTransient
    public List<Espacios> getEspaciosList() {
        return espaciosList;
    }

    public void setEspaciosList(List<Espacios> espaciosList) {
        this.espaciosList = espaciosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (usuCedula != null ? usuCedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.usuCedula == null && other.usuCedula != null) || (this.usuCedula != null && !this.usuCedula.equals(other.usuCedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Usuario[ usuCedula=" + usuCedula + " ]";
    }
    
}
