/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "espacios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Espacios.findAll", query = "SELECT e FROM Espacios e"),
    @NamedQuery(name = "Espacios.findByEspId", query = "SELECT e FROM Espacios e WHERE e.espId = :espId"),
    @NamedQuery(name = "Espacios.findByEspNombre", query = "SELECT e FROM Espacios e WHERE e.espNombre = :espNombre"),
    @NamedQuery(name = "Espacios.findByEspDispo", query = "SELECT e FROM Espacios e WHERE e.espDispo = :espDispo"),
    @NamedQuery(name = "Espacios.findByEspDetalle", query = "SELECT e FROM Espacios e WHERE e.espDetalle = :espDetalle"),
    @NamedQuery(name = "Espacios.findByEspCupo", query = "SELECT e FROM Espacios e WHERE e.espCupo = :espCupo"),
    @NamedQuery(name = "Espacios.findByEspEstado", query = "SELECT e FROM Espacios e WHERE e.espEstado = :espEstado")})
public class Espacios implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "esp_id")
    private Short espId;
    @Basic(optional = false)
    @Column(name = "esp_nombre")
    private String espNombre;
    @Basic(optional = false)
    @Column(name = "esp_dispo")
    private String espDispo;
    @Column(name = "esp_detalle")
    private String espDetalle;
    @Basic(optional = false)
    @Column(name = "esp_cupo")
    private short espCupo;
    @Column(name = "esp_estado")
    private String espEstado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evEspEspId")
    private List<AsignaEspacio> asignaEspacioList;
    @JoinColumn(name = "esp_admin", referencedColumnName = "usu_cedula")
    @ManyToOne(optional = false)
    private Usuario espAdmin;

    public Espacios() {
    }

    public Espacios(Short espId) {
        this.espId = espId;
    }

    public Espacios(Short espId, String espNombre, String espDispo, short espCupo) {
        this.espId = espId;
        this.espNombre = espNombre;
        this.espDispo = espDispo;
        this.espCupo = espCupo;
    }

    public Short getEspId() {
        return espId;
    }

    public void setEspId(Short espId) {
        this.espId = espId;
    }

    public String getEspNombre() {
        return espNombre;
    }

    public void setEspNombre(String espNombre) {
        this.espNombre = espNombre;
    }

    public String getEspDispo() {
        return espDispo;
    }

    public void setEspDispo(String espDispo) {
        this.espDispo = espDispo;
    }

    public String getEspDetalle() {
        return espDetalle;
    }

    public void setEspDetalle(String espDetalle) {
        this.espDetalle = espDetalle;
    }

    public short getEspCupo() {
        return espCupo;
    }

    public void setEspCupo(short espCupo) {
        this.espCupo = espCupo;
    }

    public String getEspEstado() {
        return espEstado;
    }

    public void setEspEstado(String espEstado) {
        this.espEstado = espEstado;
    }

    @XmlTransient
    public List<AsignaEspacio> getAsignaEspacioList() {
        return asignaEspacioList;
    }

    public void setAsignaEspacioList(List<AsignaEspacio> asignaEspacioList) {
        this.asignaEspacioList = asignaEspacioList;
    }

    public Usuario getEspAdmin() {
        return espAdmin;
    }

    public void setEspAdmin(Usuario espAdmin) {
        this.espAdmin = espAdmin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (espId != null ? espId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Espacios)) {
            return false;
        }
        Espacios other = (Espacios) object;
        if ((this.espId == null && other.espId != null) || (this.espId != null && !this.espId.equals(other.espId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Espacios[ espId=" + espId + " ]";
    }
    
}
