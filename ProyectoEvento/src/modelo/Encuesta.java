/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "encuesta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Encuesta.findAll", query = "SELECT e FROM Encuesta e"),
    @NamedQuery(name = "Encuesta.findByEncId", query = "SELECT e FROM Encuesta e WHERE e.encId = :encId"),
    @NamedQuery(name = "Encuesta.findByEncPgAbierta", query = "SELECT e FROM Encuesta e WHERE e.encPgAbierta = :encPgAbierta"),
    @NamedQuery(name = "Encuesta.findByEncPg1", query = "SELECT e FROM Encuesta e WHERE e.encPg1 = :encPg1"),
    @NamedQuery(name = "Encuesta.findByEncPg2", query = "SELECT e FROM Encuesta e WHERE e.encPg2 = :encPg2"),
    @NamedQuery(name = "Encuesta.findByEncPg3", query = "SELECT e FROM Encuesta e WHERE e.encPg3 = :encPg3"),
    @NamedQuery(name = "Encuesta.findByEncPg4", query = "SELECT e FROM Encuesta e WHERE e.encPg4 = :encPg4"),
    @NamedQuery(name = "Encuesta.findByEncPg5", query = "SELECT e FROM Encuesta e WHERE e.encPg5 = :encPg5"),
    @NamedQuery(name = "Encuesta.findByEncPg6", query = "SELECT e FROM Encuesta e WHERE e.encPg6 = :encPg6"),
    @NamedQuery(name = "Encuesta.findByEncPg7", query = "SELECT e FROM Encuesta e WHERE e.encPg7 = :encPg7"),
    @NamedQuery(name = "Encuesta.findByEncTotal", query = "SELECT e FROM Encuesta e WHERE e.encTotal = :encTotal"),
    @NamedQuery(name = "Encuesta.findByEncFecha", query = "SELECT e FROM Encuesta e WHERE e.encFecha = :encFecha")})
public class Encuesta implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "enc_id")
    private Short encId;
    @Basic(optional = false)
    @Column(name = "enc_pg_abierta")
    private int encPgAbierta;
    @Basic(optional = false)
    @Column(name = "enc_pg1")
    private int encPg1;
    @Basic(optional = false)
    @Column(name = "enc_pg2")
    private int encPg2;
    @Basic(optional = false)
    @Column(name = "enc_pg3")
    private int encPg3;
    @Basic(optional = false)
    @Column(name = "enc_pg4")
    private int encPg4;
    @Basic(optional = false)
    @Column(name = "enc_pg5")
    private int encPg5;
    @Basic(optional = false)
    @Column(name = "enc_pg6")
    private int encPg6;
    @Basic(optional = false)
    @Column(name = "enc_pg7")
    private int encPg7;
    @Basic(optional = false)
    @Column(name = "enc_total")
    private int encTotal;
    @Basic(optional = false)
    @Column(name = "enc_fecha")
    @Temporal(TemporalType.DATE)
    private Date encFecha;
    @JoinColumn(name = "enc_ev_id", referencedColumnName = "ev_id")
    @ManyToOne(optional = false)
    private Evento encEvId;
    @JoinColumn(name = "enc_usu_cedula", referencedColumnName = "usu_cedula")
    @ManyToOne(optional = false)
    private Usuario encUsuCedula;

    public Encuesta() {
    }

    public Encuesta(Short encId) {
        this.encId = encId;
    }

    public Encuesta(Short encId, int encPgAbierta, int encPg1, int encPg2, int encPg3, int encPg4, int encPg5, int encPg6, int encPg7, int encTotal, Date encFecha) {
        this.encId = encId;
        this.encPgAbierta = encPgAbierta;
        this.encPg1 = encPg1;
        this.encPg2 = encPg2;
        this.encPg3 = encPg3;
        this.encPg4 = encPg4;
        this.encPg5 = encPg5;
        this.encPg6 = encPg6;
        this.encPg7 = encPg7;
        this.encTotal = encTotal;
        this.encFecha = encFecha;
    }

    public Short getEncId() {
        return encId;
    }

    public void setEncId(Short encId) {
        this.encId = encId;
    }

    public int getEncPgAbierta() {
        return encPgAbierta;
    }

    public void setEncPgAbierta(int encPgAbierta) {
        this.encPgAbierta = encPgAbierta;
    }

    public int getEncPg1() {
        return encPg1;
    }

    public void setEncPg1(int encPg1) {
        this.encPg1 = encPg1;
    }

    public int getEncPg2() {
        return encPg2;
    }

    public void setEncPg2(int encPg2) {
        this.encPg2 = encPg2;
    }

    public int getEncPg3() {
        return encPg3;
    }

    public void setEncPg3(int encPg3) {
        this.encPg3 = encPg3;
    }

    public int getEncPg4() {
        return encPg4;
    }

    public void setEncPg4(int encPg4) {
        this.encPg4 = encPg4;
    }

    public int getEncPg5() {
        return encPg5;
    }

    public void setEncPg5(int encPg5) {
        this.encPg5 = encPg5;
    }

    public int getEncPg6() {
        return encPg6;
    }

    public void setEncPg6(int encPg6) {
        this.encPg6 = encPg6;
    }

    public int getEncPg7() {
        return encPg7;
    }

    public void setEncPg7(int encPg7) {
        this.encPg7 = encPg7;
    }

    public int getEncTotal() {
        return encTotal;
    }

    public void setEncTotal(int encTotal) {
        this.encTotal = encTotal;
    }

    public Date getEncFecha() {
        return encFecha;
    }

    public void setEncFecha(Date encFecha) {
        this.encFecha = encFecha;
    }

    public Evento getEncEvId() {
        return encEvId;
    }

    public void setEncEvId(Evento encEvId) {
        this.encEvId = encEvId;
    }

    public Usuario getEncUsuCedula() {
        return encUsuCedula;
    }

    public void setEncUsuCedula(Usuario encUsuCedula) {
        this.encUsuCedula = encUsuCedula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (encId != null ? encId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Encuesta)) {
            return false;
        }
        Encuesta other = (Encuesta) object;
        if ((this.encId == null && other.encId != null) || (this.encId != null && !this.encId.equals(other.encId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Encuesta[ encId=" + encId + " ]";
    }
    
}
