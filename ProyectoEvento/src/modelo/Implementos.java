/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "implementos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Implementos.findAll", query = "SELECT i FROM Implementos i"),
    @NamedQuery(name = "Implementos.findByImpId", query = "SELECT i FROM Implementos i WHERE i.impId = :impId"),
    @NamedQuery(name = "Implementos.findByImpFecCrea", query = "SELECT i FROM Implementos i WHERE i.impFecCrea = :impFecCrea"),
    @NamedQuery(name = "Implementos.findByImpCate", query = "SELECT i FROM Implementos i WHERE i.impCate = :impCate"),
    @NamedQuery(name = "Implementos.findByImpNombre", query = "SELECT i FROM Implementos i WHERE i.impNombre = :impNombre"),
    @NamedQuery(name = "Implementos.findByImpDispo", query = "SELECT i FROM Implementos i WHERE i.impDispo = :impDispo"),
    @NamedQuery(name = "Implementos.findByImpDetalle", query = "SELECT i FROM Implementos i WHERE i.impDetalle = :impDetalle"),
    @NamedQuery(name = "Implementos.findByImpEstado", query = "SELECT i FROM Implementos i WHERE i.impEstado = :impEstado")})
public class Implementos implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "imp_id")
    private Integer impId;
    @Basic(optional = false)
    @Column(name = "imp_fec_crea")
    @Temporal(TemporalType.DATE)
    private Date impFecCrea;
    @Basic(optional = false)
    @Column(name = "imp_cate")
    private String impCate;
    @Basic(optional = false)
    @Column(name = "imp_nombre")
    private String impNombre;
    @Basic(optional = false)
    @Column(name = "imp_dispo")
    private String impDispo;
    @Column(name = "imp_detalle")
    private String impDetalle;
    @Column(name = "imp_estado")
    private String impEstado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evImpleImpId")
    private List<AsignaImple> asignaImpleList;
    @JoinColumn(name = "imp_almacenista", referencedColumnName = "usu_cedula")
    @ManyToOne(optional = false)
    private Usuario impAlmacenista;

    public Implementos() {
    }

    public Implementos(Integer impId) {
        this.impId = impId;
    }

    public Implementos(Integer impId, Date impFecCrea, String impCate, String impNombre, String impDispo) {
        this.impId = impId;
        this.impFecCrea = impFecCrea;
        this.impCate = impCate;
        this.impNombre = impNombre;
        this.impDispo = impDispo;
    }

    public Integer getImpId() {
        return impId;
    }

    public void setImpId(Integer impId) {
        this.impId = impId;
    }

    public Date getImpFecCrea() {
        return impFecCrea;
    }

    public void setImpFecCrea(Date impFecCrea) {
        this.impFecCrea = impFecCrea;
    }

    public String getImpCate() {
        return impCate;
    }

    public void setImpCate(String impCate) {
        this.impCate = impCate;
    }

    public String getImpNombre() {
        return impNombre;
    }

    public void setImpNombre(String impNombre) {
        this.impNombre = impNombre;
    }

    public String getImpDispo() {
        return impDispo;
    }

    public void setImpDispo(String impDispo) {
        this.impDispo = impDispo;
    }

    public String getImpDetalle() {
        return impDetalle;
    }

    public void setImpDetalle(String impDetalle) {
        this.impDetalle = impDetalle;
    }

    public String getImpEstado() {
        return impEstado;
    }

    public void setImpEstado(String impEstado) {
        this.impEstado = impEstado;
    }

    @XmlTransient
    public List<AsignaImple> getAsignaImpleList() {
        return asignaImpleList;
    }

    public void setAsignaImpleList(List<AsignaImple> asignaImpleList) {
        this.asignaImpleList = asignaImpleList;
    }

    public Usuario getImpAlmacenista() {
        return impAlmacenista;
    }

    public void setImpAlmacenista(Usuario impAlmacenista) {
        this.impAlmacenista = impAlmacenista;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (impId != null ? impId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Implementos)) {
            return false;
        }
        Implementos other = (Implementos) object;
        if ((this.impId == null && other.impId != null) || (this.impId != null && !this.impId.equals(other.impId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Implementos[ impId=" + impId + " ]";
    }
    
}
