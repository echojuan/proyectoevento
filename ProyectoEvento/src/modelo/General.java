/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "general" , schema = "eventos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "General.findAll", query = "SELECT g FROM General g"),
    @NamedQuery(name = "General.findByGenId", query = "SELECT g FROM General g WHERE g.genId = :genId"),
    @NamedQuery(name = "General.findByGenNombre", query = "SELECT g FROM General g WHERE g.genNombre = :genNombre"),
    @NamedQuery(name = "General.findByGenCategoria", query = "SELECT g FROM General g WHERE g.genCategoria = :genCategoria")})
public class General implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "gen_id")
    private String genId;
    @Basic(optional = false)
    @Column(name = "gen_nombre")
    private String genNombre;
    @Basic(optional = false)
    @Column(name = "gen_categoria")
    private String genCategoria;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evCodEve")
    private List<Evento> eventoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "evCodDepe")
    private List<Evento> eventoList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuTipoGenId")
    private List<Usuario> usuarioList;

    public General() {
    }

    public General(String genId) {
        this.genId = genId;
    }

    public General(String genId, String genNombre, String genCategoria) {
        this.genId = genId;
        this.genNombre = genNombre;
        this.genCategoria = genCategoria;
    }

    public String getGenId() {
        return genId;
    }

    public void setGenId(String genId) {
        this.genId = genId;
    }

    public String getGenNombre() {
        return genNombre;
    }

    public void setGenNombre(String genNombre) {
        this.genNombre = genNombre;
    }

    public String getGenCategoria() {
        return genCategoria;
    }

    public void setGenCategoria(String genCategoria) {
        this.genCategoria = genCategoria;
    }

    @XmlTransient
    public List<Evento> getEventoList() {
        return eventoList;
    }

    public void setEventoList(List<Evento> eventoList) {
        this.eventoList = eventoList;
    }

    @XmlTransient
    public List<Evento> getEventoList1() {
        return eventoList1;
    }

    public void setEventoList1(List<Evento> eventoList1) {
        this.eventoList1 = eventoList1;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (genId != null ? genId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof General)) {
            return false;
        }
        General other = (General) object;
        if ((this.genId == null && other.genId != null) || (this.genId != null && !this.genId.equals(other.genId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.General[ genId=" + genId + " ]";
    }
    
}
