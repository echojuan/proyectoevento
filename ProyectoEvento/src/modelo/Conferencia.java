/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "conferencia")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conferencia.findAll", query = "SELECT c FROM Conferencia c"),
    @NamedQuery(name = "Conferencia.findByConfId", query = "SELECT c FROM Conferencia c WHERE c.conferenciaPK.confId = :confId"),
    @NamedQuery(name = "Conferencia.findByConfEvId", query = "SELECT c FROM Conferencia c WHERE c.conferenciaPK.confEvId = :confEvId"),
    @NamedQuery(name = "Conferencia.findByConfCeduConfere", query = "SELECT c FROM Conferencia c WHERE c.conferenciaPK.confCeduConfere = :confCeduConfere"),
    @NamedQuery(name = "Conferencia.findByConfConferencia", query = "SELECT c FROM Conferencia c WHERE c.confConferencia = :confConferencia"),
    @NamedQuery(name = "Conferencia.findByConfFecConfe", query = "SELECT c FROM Conferencia c WHERE c.confFecConfe = :confFecConfe")})
public class Conferencia implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConferenciaPK conferenciaPK;
    @Column(name = "conf_conferencia")
    private String confConferencia;
    @Basic(optional = false)
    @Column(name = "conf_fec_confe")
    @Temporal(TemporalType.DATE)
    private Date confFecConfe;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "conferencia")
    private List<Incripcion> incripcionList;
    @JoinColumn(name = "conf_cedu_confere", referencedColumnName = "con_cedula", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Conferencista conferencista;
    @JoinColumn(name = "conf_ev_id", referencedColumnName = "ev_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Evento evento;

    public Conferencia() {
    }

    public Conferencia(ConferenciaPK conferenciaPK) {
        this.conferenciaPK = conferenciaPK;
    }

    public Conferencia(ConferenciaPK conferenciaPK, Date confFecConfe) {
        this.conferenciaPK = conferenciaPK;
        this.confFecConfe = confFecConfe;
    }

    public Conferencia(int confId, int confEvId, String confCeduConfere) {
        this.conferenciaPK = new ConferenciaPK(confId, confEvId, confCeduConfere);
    }

    public ConferenciaPK getConferenciaPK() {
        return conferenciaPK;
    }

    public void setConferenciaPK(ConferenciaPK conferenciaPK) {
        this.conferenciaPK = conferenciaPK;
    }

    public String getConfConferencia() {
        return confConferencia;
    }

    public void setConfConferencia(String confConferencia) {
        this.confConferencia = confConferencia;
    }

    public Date getConfFecConfe() {
        return confFecConfe;
    }

    public void setConfFecConfe(Date confFecConfe) {
        this.confFecConfe = confFecConfe;
    }

    @XmlTransient
    public List<Incripcion> getIncripcionList() {
        return incripcionList;
    }

    public void setIncripcionList(List<Incripcion> incripcionList) {
        this.incripcionList = incripcionList;
    }

    public Conferencista getConferencista() {
        return conferencista;
    }

    public void setConferencista(Conferencista conferencista) {
        this.conferencista = conferencista;
    }

    public Evento getEvento() {
        return evento;
    }

    public void setEvento(Evento evento) {
        this.evento = evento;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conferenciaPK != null ? conferenciaPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conferencia)) {
            return false;
        }
        Conferencia other = (Conferencia) object;
        if ((this.conferenciaPK == null && other.conferenciaPK != null) || (this.conferenciaPK != null && !this.conferenciaPK.equals(other.conferenciaPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Conferencia[ conferenciaPK=" + conferenciaPK + " ]";
    }
    
}
