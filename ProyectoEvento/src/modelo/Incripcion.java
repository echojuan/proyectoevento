/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@Entity
@Table(name = "incripcion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Incripcion.findAll", query = "SELECT i FROM Incripcion i"),
    @NamedQuery(name = "Incripcion.findByIncriId", query = "SELECT i FROM Incripcion i WHERE i.incriId = :incriId"),
    @NamedQuery(name = "Incripcion.findByEvUsuAsisten", query = "SELECT i FROM Incripcion i WHERE i.evUsuAsisten = :evUsuAsisten"),
    @NamedQuery(name = "Incripcion.findByEvUsuEstado", query = "SELECT i FROM Incripcion i WHERE i.evUsuEstado = :evUsuEstado"),
    @NamedQuery(name = "Incripcion.findByEvUsuFecInscri", query = "SELECT i FROM Incripcion i WHERE i.evUsuFecInscri = :evUsuFecInscri")})
public class Incripcion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "incri_id")
    private Long incriId;
    @Basic(optional = false)
    @Column(name = "ev_usu_asisten")
    private String evUsuAsisten;
    @Basic(optional = false)
    @Column(name = "ev_usu_estado")
    private String evUsuEstado;
    @Basic(optional = false)
    @Column(name = "ev_usu_fec_inscri")
    @Temporal(TemporalType.DATE)
    private Date evUsuFecInscri;
    @JoinColumns({
        @JoinColumn(name = "ev_usu_conf_conf_id", referencedColumnName = "conf_id"),
        @JoinColumn(name = "ev_usu_conf_ev_id", referencedColumnName = "conf_ev_id"),
        @JoinColumn(name = "ev_usu_conf_cedu_confere", referencedColumnName = "conf_cedu_confere")})
    @ManyToOne(optional = false)
    private Conferencia conferencia;
    @JoinColumn(name = "ev_usu_usu_cedula", referencedColumnName = "usu_cedula")
    @ManyToOne(optional = false)
    private Usuario evUsuUsuCedula;

    public Incripcion() {
    }

    public Incripcion(Long incriId) {
        this.incriId = incriId;
    }

    public Incripcion(Long incriId, String evUsuAsisten, String evUsuEstado, Date evUsuFecInscri) {
        this.incriId = incriId;
        this.evUsuAsisten = evUsuAsisten;
        this.evUsuEstado = evUsuEstado;
        this.evUsuFecInscri = evUsuFecInscri;
    }

    public Long getIncriId() {
        return incriId;
    }

    public void setIncriId(Long incriId) {
        this.incriId = incriId;
    }

    public String getEvUsuAsisten() {
        return evUsuAsisten;
    }

    public void setEvUsuAsisten(String evUsuAsisten) {
        this.evUsuAsisten = evUsuAsisten;
    }

    public String getEvUsuEstado() {
        return evUsuEstado;
    }

    public void setEvUsuEstado(String evUsuEstado) {
        this.evUsuEstado = evUsuEstado;
    }

    public Date getEvUsuFecInscri() {
        return evUsuFecInscri;
    }

    public void setEvUsuFecInscri(Date evUsuFecInscri) {
        this.evUsuFecInscri = evUsuFecInscri;
    }

    public Conferencia getConferencia() {
        return conferencia;
    }

    public void setConferencia(Conferencia conferencia) {
        this.conferencia = conferencia;
    }

    public Usuario getEvUsuUsuCedula() {
        return evUsuUsuCedula;
    }

    public void setEvUsuUsuCedula(Usuario evUsuUsuCedula) {
        this.evUsuUsuCedula = evUsuUsuCedula;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incriId != null ? incriId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Incripcion)) {
            return false;
        }
        Incripcion other = (Incripcion) object;
        if ((this.incriId == null && other.incriId != null) || (this.incriId != null && !this.incriId.equals(other.incriId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Incripcion[ incriId=" + incriId + " ]";
    }
    
}
