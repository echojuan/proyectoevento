/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Conferencia;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.Conferencista;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;
import persistencia.exceptions.PreexistingEntityException;

/**
 *
 * @author Usuario
 */
public class ConferencistaJpaController implements Serializable {

    public ConferencistaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Conferencista conferencista) throws PreexistingEntityException, Exception {
        if (conferencista.getConferenciaList() == null) {
            conferencista.setConferenciaList(new ArrayList<Conferencia>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Conferencia> attachedConferenciaList = new ArrayList<Conferencia>();
            for (Conferencia conferenciaListConferenciaToAttach : conferencista.getConferenciaList()) {
                conferenciaListConferenciaToAttach = em.getReference(conferenciaListConferenciaToAttach.getClass(), conferenciaListConferenciaToAttach.getConferenciaPK());
                attachedConferenciaList.add(conferenciaListConferenciaToAttach);
            }
            conferencista.setConferenciaList(attachedConferenciaList);
            em.persist(conferencista);
            for (Conferencia conferenciaListConferencia : conferencista.getConferenciaList()) {
                Conferencista oldConferencistaOfConferenciaListConferencia = conferenciaListConferencia.getConferencista();
                conferenciaListConferencia.setConferencista(conferencista);
                conferenciaListConferencia = em.merge(conferenciaListConferencia);
                if (oldConferencistaOfConferenciaListConferencia != null) {
                    oldConferencistaOfConferenciaListConferencia.getConferenciaList().remove(conferenciaListConferencia);
                    oldConferencistaOfConferenciaListConferencia = em.merge(oldConferencistaOfConferenciaListConferencia);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConferencista(conferencista.getConCedula()) != null) {
                throw new PreexistingEntityException("Conferencista " + conferencista + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Conferencista conferencista) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conferencista persistentConferencista = em.find(Conferencista.class, conferencista.getConCedula());
            List<Conferencia> conferenciaListOld = persistentConferencista.getConferenciaList();
            List<Conferencia> conferenciaListNew = conferencista.getConferenciaList();
            List<String> illegalOrphanMessages = null;
            for (Conferencia conferenciaListOldConferencia : conferenciaListOld) {
                if (!conferenciaListNew.contains(conferenciaListOldConferencia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Conferencia " + conferenciaListOldConferencia + " since its conferencista field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Conferencia> attachedConferenciaListNew = new ArrayList<Conferencia>();
            for (Conferencia conferenciaListNewConferenciaToAttach : conferenciaListNew) {
                conferenciaListNewConferenciaToAttach = em.getReference(conferenciaListNewConferenciaToAttach.getClass(), conferenciaListNewConferenciaToAttach.getConferenciaPK());
                attachedConferenciaListNew.add(conferenciaListNewConferenciaToAttach);
            }
            conferenciaListNew = attachedConferenciaListNew;
            conferencista.setConferenciaList(conferenciaListNew);
            conferencista = em.merge(conferencista);
            for (Conferencia conferenciaListNewConferencia : conferenciaListNew) {
                if (!conferenciaListOld.contains(conferenciaListNewConferencia)) {
                    Conferencista oldConferencistaOfConferenciaListNewConferencia = conferenciaListNewConferencia.getConferencista();
                    conferenciaListNewConferencia.setConferencista(conferencista);
                    conferenciaListNewConferencia = em.merge(conferenciaListNewConferencia);
                    if (oldConferencistaOfConferenciaListNewConferencia != null && !oldConferencistaOfConferenciaListNewConferencia.equals(conferencista)) {
                        oldConferencistaOfConferenciaListNewConferencia.getConferenciaList().remove(conferenciaListNewConferencia);
                        oldConferencistaOfConferenciaListNewConferencia = em.merge(oldConferencistaOfConferenciaListNewConferencia);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = conferencista.getConCedula();
                if (findConferencista(id) == null) {
                    throw new NonexistentEntityException("The conferencista with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conferencista conferencista;
            try {
                conferencista = em.getReference(Conferencista.class, id);
                conferencista.getConCedula();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The conferencista with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Conferencia> conferenciaListOrphanCheck = conferencista.getConferenciaList();
            for (Conferencia conferenciaListOrphanCheckConferencia : conferenciaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Conferencista (" + conferencista + ") cannot be destroyed since the Conferencia " + conferenciaListOrphanCheckConferencia + " in its conferenciaList field has a non-nullable conferencista field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(conferencista);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Conferencista> findConferencistaEntities() {
        return findConferencistaEntities(true, -1, -1);
    }

    public List<Conferencista> findConferencistaEntities(int maxResults, int firstResult) {
        return findConferencistaEntities(false, maxResults, firstResult);
    }

    private List<Conferencista> findConferencistaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Conferencista.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Conferencista findConferencista(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Conferencista.class, id);
        } finally {
            em.close();
        }
    }

    public int getConferencistaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Conferencista> rt = cq.from(Conferencista.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
