/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Conferencista;
import modelo.Evento;
import modelo.Incripcion;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.Conferencia;
import modelo.ConferenciaPK;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;
import persistencia.exceptions.PreexistingEntityException;

/**
 *
 * @author Usuario
 */
public class ConferenciaJpaController implements Serializable {

    public ConferenciaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Conferencia conferencia) throws PreexistingEntityException, Exception {
        if (conferencia.getConferenciaPK() == null) {
            conferencia.setConferenciaPK(new ConferenciaPK());
        }
        if (conferencia.getIncripcionList() == null) {
            conferencia.setIncripcionList(new ArrayList<Incripcion>());
        }
        conferencia.getConferenciaPK().setConfCeduConfere(conferencia.getConferencista().getConCedula());
        conferencia.getConferenciaPK().setConfEvId(conferencia.getEvento().getEvId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conferencista conferencista = conferencia.getConferencista();
            if (conferencista != null) {
                conferencista = em.getReference(conferencista.getClass(), conferencista.getConCedula());
                conferencia.setConferencista(conferencista);
            }
            Evento evento = conferencia.getEvento();
            if (evento != null) {
                evento = em.getReference(evento.getClass(), evento.getEvId());
                conferencia.setEvento(evento);
            }
            List<Incripcion> attachedIncripcionList = new ArrayList<Incripcion>();
            for (Incripcion incripcionListIncripcionToAttach : conferencia.getIncripcionList()) {
                incripcionListIncripcionToAttach = em.getReference(incripcionListIncripcionToAttach.getClass(), incripcionListIncripcionToAttach.getIncriId());
                attachedIncripcionList.add(incripcionListIncripcionToAttach);
            }
            conferencia.setIncripcionList(attachedIncripcionList);
            em.persist(conferencia);
            if (conferencista != null) {
                conferencista.getConferenciaList().add(conferencia);
                conferencista = em.merge(conferencista);
            }
            if (evento != null) {
                evento.getConferenciaList().add(conferencia);
                evento = em.merge(evento);
            }
            for (Incripcion incripcionListIncripcion : conferencia.getIncripcionList()) {
                Conferencia oldConferenciaOfIncripcionListIncripcion = incripcionListIncripcion.getConferencia();
                incripcionListIncripcion.setConferencia(conferencia);
                incripcionListIncripcion = em.merge(incripcionListIncripcion);
                if (oldConferenciaOfIncripcionListIncripcion != null) {
                    oldConferenciaOfIncripcionListIncripcion.getIncripcionList().remove(incripcionListIncripcion);
                    oldConferenciaOfIncripcionListIncripcion = em.merge(oldConferenciaOfIncripcionListIncripcion);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findConferencia(conferencia.getConferenciaPK()) != null) {
                throw new PreexistingEntityException("Conferencia " + conferencia + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Conferencia conferencia) throws IllegalOrphanException, NonexistentEntityException, Exception {
        conferencia.getConferenciaPK().setConfCeduConfere(conferencia.getConferencista().getConCedula());
        conferencia.getConferenciaPK().setConfEvId(conferencia.getEvento().getEvId());
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conferencia persistentConferencia = em.find(Conferencia.class, conferencia.getConferenciaPK());
            Conferencista conferencistaOld = persistentConferencia.getConferencista();
            Conferencista conferencistaNew = conferencia.getConferencista();
            Evento eventoOld = persistentConferencia.getEvento();
            Evento eventoNew = conferencia.getEvento();
            List<Incripcion> incripcionListOld = persistentConferencia.getIncripcionList();
            List<Incripcion> incripcionListNew = conferencia.getIncripcionList();
            List<String> illegalOrphanMessages = null;
            for (Incripcion incripcionListOldIncripcion : incripcionListOld) {
                if (!incripcionListNew.contains(incripcionListOldIncripcion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Incripcion " + incripcionListOldIncripcion + " since its conferencia field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (conferencistaNew != null) {
                conferencistaNew = em.getReference(conferencistaNew.getClass(), conferencistaNew.getConCedula());
                conferencia.setConferencista(conferencistaNew);
            }
            if (eventoNew != null) {
                eventoNew = em.getReference(eventoNew.getClass(), eventoNew.getEvId());
                conferencia.setEvento(eventoNew);
            }
            List<Incripcion> attachedIncripcionListNew = new ArrayList<Incripcion>();
            for (Incripcion incripcionListNewIncripcionToAttach : incripcionListNew) {
                incripcionListNewIncripcionToAttach = em.getReference(incripcionListNewIncripcionToAttach.getClass(), incripcionListNewIncripcionToAttach.getIncriId());
                attachedIncripcionListNew.add(incripcionListNewIncripcionToAttach);
            }
            incripcionListNew = attachedIncripcionListNew;
            conferencia.setIncripcionList(incripcionListNew);
            conferencia = em.merge(conferencia);
            if (conferencistaOld != null && !conferencistaOld.equals(conferencistaNew)) {
                conferencistaOld.getConferenciaList().remove(conferencia);
                conferencistaOld = em.merge(conferencistaOld);
            }
            if (conferencistaNew != null && !conferencistaNew.equals(conferencistaOld)) {
                conferencistaNew.getConferenciaList().add(conferencia);
                conferencistaNew = em.merge(conferencistaNew);
            }
            if (eventoOld != null && !eventoOld.equals(eventoNew)) {
                eventoOld.getConferenciaList().remove(conferencia);
                eventoOld = em.merge(eventoOld);
            }
            if (eventoNew != null && !eventoNew.equals(eventoOld)) {
                eventoNew.getConferenciaList().add(conferencia);
                eventoNew = em.merge(eventoNew);
            }
            for (Incripcion incripcionListNewIncripcion : incripcionListNew) {
                if (!incripcionListOld.contains(incripcionListNewIncripcion)) {
                    Conferencia oldConferenciaOfIncripcionListNewIncripcion = incripcionListNewIncripcion.getConferencia();
                    incripcionListNewIncripcion.setConferencia(conferencia);
                    incripcionListNewIncripcion = em.merge(incripcionListNewIncripcion);
                    if (oldConferenciaOfIncripcionListNewIncripcion != null && !oldConferenciaOfIncripcionListNewIncripcion.equals(conferencia)) {
                        oldConferenciaOfIncripcionListNewIncripcion.getIncripcionList().remove(incripcionListNewIncripcion);
                        oldConferenciaOfIncripcionListNewIncripcion = em.merge(oldConferenciaOfIncripcionListNewIncripcion);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                ConferenciaPK id = conferencia.getConferenciaPK();
                if (findConferencia(id) == null) {
                    throw new NonexistentEntityException("The conferencia with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(ConferenciaPK id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conferencia conferencia;
            try {
                conferencia = em.getReference(Conferencia.class, id);
                conferencia.getConferenciaPK();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The conferencia with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Incripcion> incripcionListOrphanCheck = conferencia.getIncripcionList();
            for (Incripcion incripcionListOrphanCheckIncripcion : incripcionListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Conferencia (" + conferencia + ") cannot be destroyed since the Incripcion " + incripcionListOrphanCheckIncripcion + " in its incripcionList field has a non-nullable conferencia field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Conferencista conferencista = conferencia.getConferencista();
            if (conferencista != null) {
                conferencista.getConferenciaList().remove(conferencia);
                conferencista = em.merge(conferencista);
            }
            Evento evento = conferencia.getEvento();
            if (evento != null) {
                evento.getConferenciaList().remove(conferencia);
                evento = em.merge(evento);
            }
            em.remove(conferencia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Conferencia> findConferenciaEntities() {
        return findConferenciaEntities(true, -1, -1);
    }

    public List<Conferencia> findConferenciaEntities(int maxResults, int firstResult) {
        return findConferenciaEntities(false, maxResults, firstResult);
    }

    private List<Conferencia> findConferenciaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Conferencia.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Conferencia findConferencia(ConferenciaPK id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Conferencia.class, id);
        } finally {
            em.close();
        }
    }

    public int getConferenciaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Conferencia> rt = cq.from(Conferencia.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
