/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.General;
import modelo.AsignaImple;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.AsignaEspacio;
import modelo.Encuesta;
import modelo.Conferencia;
import modelo.Evento;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class EventoJpaController implements Serializable {

    public EventoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Evento evento) {
        if (evento.getAsignaImpleList() == null) {
            evento.setAsignaImpleList(new ArrayList<AsignaImple>());
        }
        if (evento.getAsignaEspacioList() == null) {
            evento.setAsignaEspacioList(new ArrayList<AsignaEspacio>());
        }
        if (evento.getEncuestaList() == null) {
            evento.setEncuestaList(new ArrayList<Encuesta>());
        }
        if (evento.getConferenciaList() == null) {
            evento.setConferenciaList(new ArrayList<Conferencia>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            General evCodEve = evento.getEvCodEve();
            if (evCodEve != null) {
                evCodEve = em.getReference(evCodEve.getClass(), evCodEve.getGenId());
                evento.setEvCodEve(evCodEve);
            }
            General evCodDepe = evento.getEvCodDepe();
            if (evCodDepe != null) {
                evCodDepe = em.getReference(evCodDepe.getClass(), evCodDepe.getGenId());
                evento.setEvCodDepe(evCodDepe);
            }
            List<AsignaImple> attachedAsignaImpleList = new ArrayList<AsignaImple>();
            for (AsignaImple asignaImpleListAsignaImpleToAttach : evento.getAsignaImpleList()) {
                asignaImpleListAsignaImpleToAttach = em.getReference(asignaImpleListAsignaImpleToAttach.getClass(), asignaImpleListAsignaImpleToAttach.getEvImpleId());
                attachedAsignaImpleList.add(asignaImpleListAsignaImpleToAttach);
            }
            evento.setAsignaImpleList(attachedAsignaImpleList);
            List<AsignaEspacio> attachedAsignaEspacioList = new ArrayList<AsignaEspacio>();
            for (AsignaEspacio asignaEspacioListAsignaEspacioToAttach : evento.getAsignaEspacioList()) {
                asignaEspacioListAsignaEspacioToAttach = em.getReference(asignaEspacioListAsignaEspacioToAttach.getClass(), asignaEspacioListAsignaEspacioToAttach.getEvEspId());
                attachedAsignaEspacioList.add(asignaEspacioListAsignaEspacioToAttach);
            }
            evento.setAsignaEspacioList(attachedAsignaEspacioList);
            List<Encuesta> attachedEncuestaList = new ArrayList<Encuesta>();
            for (Encuesta encuestaListEncuestaToAttach : evento.getEncuestaList()) {
                encuestaListEncuestaToAttach = em.getReference(encuestaListEncuestaToAttach.getClass(), encuestaListEncuestaToAttach.getEncId());
                attachedEncuestaList.add(encuestaListEncuestaToAttach);
            }
            evento.setEncuestaList(attachedEncuestaList);
            List<Conferencia> attachedConferenciaList = new ArrayList<Conferencia>();
            for (Conferencia conferenciaListConferenciaToAttach : evento.getConferenciaList()) {
                conferenciaListConferenciaToAttach = em.getReference(conferenciaListConferenciaToAttach.getClass(), conferenciaListConferenciaToAttach.getConferenciaPK());
                attachedConferenciaList.add(conferenciaListConferenciaToAttach);
            }
            evento.setConferenciaList(attachedConferenciaList);
            em.persist(evento);
            if (evCodEve != null) {
                evCodEve.getEventoList().add(evento);
                evCodEve = em.merge(evCodEve);
            }
            if (evCodDepe != null) {
                evCodDepe.getEventoList().add(evento);
                evCodDepe = em.merge(evCodDepe);
            }
            for (AsignaImple asignaImpleListAsignaImple : evento.getAsignaImpleList()) {
                Evento oldEvImpleEvIdOfAsignaImpleListAsignaImple = asignaImpleListAsignaImple.getEvImpleEvId();
                asignaImpleListAsignaImple.setEvImpleEvId(evento);
                asignaImpleListAsignaImple = em.merge(asignaImpleListAsignaImple);
                if (oldEvImpleEvIdOfAsignaImpleListAsignaImple != null) {
                    oldEvImpleEvIdOfAsignaImpleListAsignaImple.getAsignaImpleList().remove(asignaImpleListAsignaImple);
                    oldEvImpleEvIdOfAsignaImpleListAsignaImple = em.merge(oldEvImpleEvIdOfAsignaImpleListAsignaImple);
                }
            }
            for (AsignaEspacio asignaEspacioListAsignaEspacio : evento.getAsignaEspacioList()) {
                Evento oldEvEspEvIdOfAsignaEspacioListAsignaEspacio = asignaEspacioListAsignaEspacio.getEvEspEvId();
                asignaEspacioListAsignaEspacio.setEvEspEvId(evento);
                asignaEspacioListAsignaEspacio = em.merge(asignaEspacioListAsignaEspacio);
                if (oldEvEspEvIdOfAsignaEspacioListAsignaEspacio != null) {
                    oldEvEspEvIdOfAsignaEspacioListAsignaEspacio.getAsignaEspacioList().remove(asignaEspacioListAsignaEspacio);
                    oldEvEspEvIdOfAsignaEspacioListAsignaEspacio = em.merge(oldEvEspEvIdOfAsignaEspacioListAsignaEspacio);
                }
            }
            for (Encuesta encuestaListEncuesta : evento.getEncuestaList()) {
                Evento oldEncEvIdOfEncuestaListEncuesta = encuestaListEncuesta.getEncEvId();
                encuestaListEncuesta.setEncEvId(evento);
                encuestaListEncuesta = em.merge(encuestaListEncuesta);
                if (oldEncEvIdOfEncuestaListEncuesta != null) {
                    oldEncEvIdOfEncuestaListEncuesta.getEncuestaList().remove(encuestaListEncuesta);
                    oldEncEvIdOfEncuestaListEncuesta = em.merge(oldEncEvIdOfEncuestaListEncuesta);
                }
            }
            for (Conferencia conferenciaListConferencia : evento.getConferenciaList()) {
                Evento oldEventoOfConferenciaListConferencia = conferenciaListConferencia.getEvento();
                conferenciaListConferencia.setEvento(evento);
                conferenciaListConferencia = em.merge(conferenciaListConferencia);
                if (oldEventoOfConferenciaListConferencia != null) {
                    oldEventoOfConferenciaListConferencia.getConferenciaList().remove(conferenciaListConferencia);
                    oldEventoOfConferenciaListConferencia = em.merge(oldEventoOfConferenciaListConferencia);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Evento evento) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Evento persistentEvento = em.find(Evento.class, evento.getEvId());
            General evCodEveOld = persistentEvento.getEvCodEve();
            General evCodEveNew = evento.getEvCodEve();
            General evCodDepeOld = persistentEvento.getEvCodDepe();
            General evCodDepeNew = evento.getEvCodDepe();
            List<AsignaImple> asignaImpleListOld = persistentEvento.getAsignaImpleList();
            List<AsignaImple> asignaImpleListNew = evento.getAsignaImpleList();
            List<AsignaEspacio> asignaEspacioListOld = persistentEvento.getAsignaEspacioList();
            List<AsignaEspacio> asignaEspacioListNew = evento.getAsignaEspacioList();
            List<Encuesta> encuestaListOld = persistentEvento.getEncuestaList();
            List<Encuesta> encuestaListNew = evento.getEncuestaList();
            List<Conferencia> conferenciaListOld = persistentEvento.getConferenciaList();
            List<Conferencia> conferenciaListNew = evento.getConferenciaList();
            List<String> illegalOrphanMessages = null;
            for (AsignaImple asignaImpleListOldAsignaImple : asignaImpleListOld) {
                if (!asignaImpleListNew.contains(asignaImpleListOldAsignaImple)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AsignaImple " + asignaImpleListOldAsignaImple + " since its evImpleEvId field is not nullable.");
                }
            }
            for (AsignaEspacio asignaEspacioListOldAsignaEspacio : asignaEspacioListOld) {
                if (!asignaEspacioListNew.contains(asignaEspacioListOldAsignaEspacio)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AsignaEspacio " + asignaEspacioListOldAsignaEspacio + " since its evEspEvId field is not nullable.");
                }
            }
            for (Encuesta encuestaListOldEncuesta : encuestaListOld) {
                if (!encuestaListNew.contains(encuestaListOldEncuesta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Encuesta " + encuestaListOldEncuesta + " since its encEvId field is not nullable.");
                }
            }
            for (Conferencia conferenciaListOldConferencia : conferenciaListOld) {
                if (!conferenciaListNew.contains(conferenciaListOldConferencia)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Conferencia " + conferenciaListOldConferencia + " since its evento field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (evCodEveNew != null) {
                evCodEveNew = em.getReference(evCodEveNew.getClass(), evCodEveNew.getGenId());
                evento.setEvCodEve(evCodEveNew);
            }
            if (evCodDepeNew != null) {
                evCodDepeNew = em.getReference(evCodDepeNew.getClass(), evCodDepeNew.getGenId());
                evento.setEvCodDepe(evCodDepeNew);
            }
            List<AsignaImple> attachedAsignaImpleListNew = new ArrayList<AsignaImple>();
            for (AsignaImple asignaImpleListNewAsignaImpleToAttach : asignaImpleListNew) {
                asignaImpleListNewAsignaImpleToAttach = em.getReference(asignaImpleListNewAsignaImpleToAttach.getClass(), asignaImpleListNewAsignaImpleToAttach.getEvImpleId());
                attachedAsignaImpleListNew.add(asignaImpleListNewAsignaImpleToAttach);
            }
            asignaImpleListNew = attachedAsignaImpleListNew;
            evento.setAsignaImpleList(asignaImpleListNew);
            List<AsignaEspacio> attachedAsignaEspacioListNew = new ArrayList<AsignaEspacio>();
            for (AsignaEspacio asignaEspacioListNewAsignaEspacioToAttach : asignaEspacioListNew) {
                asignaEspacioListNewAsignaEspacioToAttach = em.getReference(asignaEspacioListNewAsignaEspacioToAttach.getClass(), asignaEspacioListNewAsignaEspacioToAttach.getEvEspId());
                attachedAsignaEspacioListNew.add(asignaEspacioListNewAsignaEspacioToAttach);
            }
            asignaEspacioListNew = attachedAsignaEspacioListNew;
            evento.setAsignaEspacioList(asignaEspacioListNew);
            List<Encuesta> attachedEncuestaListNew = new ArrayList<Encuesta>();
            for (Encuesta encuestaListNewEncuestaToAttach : encuestaListNew) {
                encuestaListNewEncuestaToAttach = em.getReference(encuestaListNewEncuestaToAttach.getClass(), encuestaListNewEncuestaToAttach.getEncId());
                attachedEncuestaListNew.add(encuestaListNewEncuestaToAttach);
            }
            encuestaListNew = attachedEncuestaListNew;
            evento.setEncuestaList(encuestaListNew);
            List<Conferencia> attachedConferenciaListNew = new ArrayList<Conferencia>();
            for (Conferencia conferenciaListNewConferenciaToAttach : conferenciaListNew) {
                conferenciaListNewConferenciaToAttach = em.getReference(conferenciaListNewConferenciaToAttach.getClass(), conferenciaListNewConferenciaToAttach.getConferenciaPK());
                attachedConferenciaListNew.add(conferenciaListNewConferenciaToAttach);
            }
            conferenciaListNew = attachedConferenciaListNew;
            evento.setConferenciaList(conferenciaListNew);
            evento = em.merge(evento);
            if (evCodEveOld != null && !evCodEveOld.equals(evCodEveNew)) {
                evCodEveOld.getEventoList().remove(evento);
                evCodEveOld = em.merge(evCodEveOld);
            }
            if (evCodEveNew != null && !evCodEveNew.equals(evCodEveOld)) {
                evCodEveNew.getEventoList().add(evento);
                evCodEveNew = em.merge(evCodEveNew);
            }
            if (evCodDepeOld != null && !evCodDepeOld.equals(evCodDepeNew)) {
                evCodDepeOld.getEventoList().remove(evento);
                evCodDepeOld = em.merge(evCodDepeOld);
            }
            if (evCodDepeNew != null && !evCodDepeNew.equals(evCodDepeOld)) {
                evCodDepeNew.getEventoList().add(evento);
                evCodDepeNew = em.merge(evCodDepeNew);
            }
            for (AsignaImple asignaImpleListNewAsignaImple : asignaImpleListNew) {
                if (!asignaImpleListOld.contains(asignaImpleListNewAsignaImple)) {
                    Evento oldEvImpleEvIdOfAsignaImpleListNewAsignaImple = asignaImpleListNewAsignaImple.getEvImpleEvId();
                    asignaImpleListNewAsignaImple.setEvImpleEvId(evento);
                    asignaImpleListNewAsignaImple = em.merge(asignaImpleListNewAsignaImple);
                    if (oldEvImpleEvIdOfAsignaImpleListNewAsignaImple != null && !oldEvImpleEvIdOfAsignaImpleListNewAsignaImple.equals(evento)) {
                        oldEvImpleEvIdOfAsignaImpleListNewAsignaImple.getAsignaImpleList().remove(asignaImpleListNewAsignaImple);
                        oldEvImpleEvIdOfAsignaImpleListNewAsignaImple = em.merge(oldEvImpleEvIdOfAsignaImpleListNewAsignaImple);
                    }
                }
            }
            for (AsignaEspacio asignaEspacioListNewAsignaEspacio : asignaEspacioListNew) {
                if (!asignaEspacioListOld.contains(asignaEspacioListNewAsignaEspacio)) {
                    Evento oldEvEspEvIdOfAsignaEspacioListNewAsignaEspacio = asignaEspacioListNewAsignaEspacio.getEvEspEvId();
                    asignaEspacioListNewAsignaEspacio.setEvEspEvId(evento);
                    asignaEspacioListNewAsignaEspacio = em.merge(asignaEspacioListNewAsignaEspacio);
                    if (oldEvEspEvIdOfAsignaEspacioListNewAsignaEspacio != null && !oldEvEspEvIdOfAsignaEspacioListNewAsignaEspacio.equals(evento)) {
                        oldEvEspEvIdOfAsignaEspacioListNewAsignaEspacio.getAsignaEspacioList().remove(asignaEspacioListNewAsignaEspacio);
                        oldEvEspEvIdOfAsignaEspacioListNewAsignaEspacio = em.merge(oldEvEspEvIdOfAsignaEspacioListNewAsignaEspacio);
                    }
                }
            }
            for (Encuesta encuestaListNewEncuesta : encuestaListNew) {
                if (!encuestaListOld.contains(encuestaListNewEncuesta)) {
                    Evento oldEncEvIdOfEncuestaListNewEncuesta = encuestaListNewEncuesta.getEncEvId();
                    encuestaListNewEncuesta.setEncEvId(evento);
                    encuestaListNewEncuesta = em.merge(encuestaListNewEncuesta);
                    if (oldEncEvIdOfEncuestaListNewEncuesta != null && !oldEncEvIdOfEncuestaListNewEncuesta.equals(evento)) {
                        oldEncEvIdOfEncuestaListNewEncuesta.getEncuestaList().remove(encuestaListNewEncuesta);
                        oldEncEvIdOfEncuestaListNewEncuesta = em.merge(oldEncEvIdOfEncuestaListNewEncuesta);
                    }
                }
            }
            for (Conferencia conferenciaListNewConferencia : conferenciaListNew) {
                if (!conferenciaListOld.contains(conferenciaListNewConferencia)) {
                    Evento oldEventoOfConferenciaListNewConferencia = conferenciaListNewConferencia.getEvento();
                    conferenciaListNewConferencia.setEvento(evento);
                    conferenciaListNewConferencia = em.merge(conferenciaListNewConferencia);
                    if (oldEventoOfConferenciaListNewConferencia != null && !oldEventoOfConferenciaListNewConferencia.equals(evento)) {
                        oldEventoOfConferenciaListNewConferencia.getConferenciaList().remove(conferenciaListNewConferencia);
                        oldEventoOfConferenciaListNewConferencia = em.merge(oldEventoOfConferenciaListNewConferencia);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = evento.getEvId();
                if (findEvento(id) == null) {
                    throw new NonexistentEntityException("The evento with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Evento evento;
            try {
                evento = em.getReference(Evento.class, id);
                evento.getEvId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The evento with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<AsignaImple> asignaImpleListOrphanCheck = evento.getAsignaImpleList();
            for (AsignaImple asignaImpleListOrphanCheckAsignaImple : asignaImpleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Evento (" + evento + ") cannot be destroyed since the AsignaImple " + asignaImpleListOrphanCheckAsignaImple + " in its asignaImpleList field has a non-nullable evImpleEvId field.");
            }
            List<AsignaEspacio> asignaEspacioListOrphanCheck = evento.getAsignaEspacioList();
            for (AsignaEspacio asignaEspacioListOrphanCheckAsignaEspacio : asignaEspacioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Evento (" + evento + ") cannot be destroyed since the AsignaEspacio " + asignaEspacioListOrphanCheckAsignaEspacio + " in its asignaEspacioList field has a non-nullable evEspEvId field.");
            }
            List<Encuesta> encuestaListOrphanCheck = evento.getEncuestaList();
            for (Encuesta encuestaListOrphanCheckEncuesta : encuestaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Evento (" + evento + ") cannot be destroyed since the Encuesta " + encuestaListOrphanCheckEncuesta + " in its encuestaList field has a non-nullable encEvId field.");
            }
            List<Conferencia> conferenciaListOrphanCheck = evento.getConferenciaList();
            for (Conferencia conferenciaListOrphanCheckConferencia : conferenciaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Evento (" + evento + ") cannot be destroyed since the Conferencia " + conferenciaListOrphanCheckConferencia + " in its conferenciaList field has a non-nullable evento field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            General evCodEve = evento.getEvCodEve();
            if (evCodEve != null) {
                evCodEve.getEventoList().remove(evento);
                evCodEve = em.merge(evCodEve);
            }
            General evCodDepe = evento.getEvCodDepe();
            if (evCodDepe != null) {
                evCodDepe.getEventoList().remove(evento);
                evCodDepe = em.merge(evCodDepe);
            }
            em.remove(evento);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Evento> findEventoEntities() {
        return findEventoEntities(true, -1, -1);
    }

    public List<Evento> findEventoEntities(int maxResults, int firstResult) {
        return findEventoEntities(false, maxResults, firstResult);
    }

    private List<Evento> findEventoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Evento.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Evento findEvento(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Evento.class, id);
        } finally {
            em.close();
        }
    }

    public int getEventoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Evento> rt = cq.from(Evento.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
