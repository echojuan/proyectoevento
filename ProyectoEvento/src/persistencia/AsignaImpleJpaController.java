/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.AsignaImple;
import modelo.Evento;
import modelo.Implementos;
import modelo.Usuario;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class AsignaImpleJpaController implements Serializable {

    public AsignaImpleJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AsignaImple asignaImple) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Evento evImpleEvId = asignaImple.getEvImpleEvId();
            if (evImpleEvId != null) {
                evImpleEvId = em.getReference(evImpleEvId.getClass(), evImpleEvId.getEvId());
                asignaImple.setEvImpleEvId(evImpleEvId);
            }
            Implementos evImpleImpId = asignaImple.getEvImpleImpId();
            if (evImpleImpId != null) {
                evImpleImpId = em.getReference(evImpleImpId.getClass(), evImpleImpId.getImpId());
                asignaImple.setEvImpleImpId(evImpleImpId);
            }
            Usuario evImpleAdmin = asignaImple.getEvImpleAdmin();
            if (evImpleAdmin != null) {
                evImpleAdmin = em.getReference(evImpleAdmin.getClass(), evImpleAdmin.getUsuCedula());
                asignaImple.setEvImpleAdmin(evImpleAdmin);
            }
            em.persist(asignaImple);
            if (evImpleEvId != null) {
                evImpleEvId.getAsignaImpleList().add(asignaImple);
                evImpleEvId = em.merge(evImpleEvId);
            }
            if (evImpleImpId != null) {
                evImpleImpId.getAsignaImpleList().add(asignaImple);
                evImpleImpId = em.merge(evImpleImpId);
            }
            if (evImpleAdmin != null) {
                evImpleAdmin.getAsignaImpleList().add(asignaImple);
                evImpleAdmin = em.merge(evImpleAdmin);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AsignaImple asignaImple) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AsignaImple persistentAsignaImple = em.find(AsignaImple.class, asignaImple.getEvImpleId());
            Evento evImpleEvIdOld = persistentAsignaImple.getEvImpleEvId();
            Evento evImpleEvIdNew = asignaImple.getEvImpleEvId();
            Implementos evImpleImpIdOld = persistentAsignaImple.getEvImpleImpId();
            Implementos evImpleImpIdNew = asignaImple.getEvImpleImpId();
            Usuario evImpleAdminOld = persistentAsignaImple.getEvImpleAdmin();
            Usuario evImpleAdminNew = asignaImple.getEvImpleAdmin();
            if (evImpleEvIdNew != null) {
                evImpleEvIdNew = em.getReference(evImpleEvIdNew.getClass(), evImpleEvIdNew.getEvId());
                asignaImple.setEvImpleEvId(evImpleEvIdNew);
            }
            if (evImpleImpIdNew != null) {
                evImpleImpIdNew = em.getReference(evImpleImpIdNew.getClass(), evImpleImpIdNew.getImpId());
                asignaImple.setEvImpleImpId(evImpleImpIdNew);
            }
            if (evImpleAdminNew != null) {
                evImpleAdminNew = em.getReference(evImpleAdminNew.getClass(), evImpleAdminNew.getUsuCedula());
                asignaImple.setEvImpleAdmin(evImpleAdminNew);
            }
            asignaImple = em.merge(asignaImple);
            if (evImpleEvIdOld != null && !evImpleEvIdOld.equals(evImpleEvIdNew)) {
                evImpleEvIdOld.getAsignaImpleList().remove(asignaImple);
                evImpleEvIdOld = em.merge(evImpleEvIdOld);
            }
            if (evImpleEvIdNew != null && !evImpleEvIdNew.equals(evImpleEvIdOld)) {
                evImpleEvIdNew.getAsignaImpleList().add(asignaImple);
                evImpleEvIdNew = em.merge(evImpleEvIdNew);
            }
            if (evImpleImpIdOld != null && !evImpleImpIdOld.equals(evImpleImpIdNew)) {
                evImpleImpIdOld.getAsignaImpleList().remove(asignaImple);
                evImpleImpIdOld = em.merge(evImpleImpIdOld);
            }
            if (evImpleImpIdNew != null && !evImpleImpIdNew.equals(evImpleImpIdOld)) {
                evImpleImpIdNew.getAsignaImpleList().add(asignaImple);
                evImpleImpIdNew = em.merge(evImpleImpIdNew);
            }
            if (evImpleAdminOld != null && !evImpleAdminOld.equals(evImpleAdminNew)) {
                evImpleAdminOld.getAsignaImpleList().remove(asignaImple);
                evImpleAdminOld = em.merge(evImpleAdminOld);
            }
            if (evImpleAdminNew != null && !evImpleAdminNew.equals(evImpleAdminOld)) {
                evImpleAdminNew.getAsignaImpleList().add(asignaImple);
                evImpleAdminNew = em.merge(evImpleAdminNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = asignaImple.getEvImpleId();
                if (findAsignaImple(id) == null) {
                    throw new NonexistentEntityException("The asignaImple with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AsignaImple asignaImple;
            try {
                asignaImple = em.getReference(AsignaImple.class, id);
                asignaImple.getEvImpleId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The asignaImple with id " + id + " no longer exists.", enfe);
            }
            Evento evImpleEvId = asignaImple.getEvImpleEvId();
            if (evImpleEvId != null) {
                evImpleEvId.getAsignaImpleList().remove(asignaImple);
                evImpleEvId = em.merge(evImpleEvId);
            }
            Implementos evImpleImpId = asignaImple.getEvImpleImpId();
            if (evImpleImpId != null) {
                evImpleImpId.getAsignaImpleList().remove(asignaImple);
                evImpleImpId = em.merge(evImpleImpId);
            }
            Usuario evImpleAdmin = asignaImple.getEvImpleAdmin();
            if (evImpleAdmin != null) {
                evImpleAdmin.getAsignaImpleList().remove(asignaImple);
                evImpleAdmin = em.merge(evImpleAdmin);
            }
            em.remove(asignaImple);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AsignaImple> findAsignaImpleEntities() {
        return findAsignaImpleEntities(true, -1, -1);
    }

    public List<AsignaImple> findAsignaImpleEntities(int maxResults, int firstResult) {
        return findAsignaImpleEntities(false, maxResults, firstResult);
    }

    private List<AsignaImple> findAsignaImpleEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AsignaImple.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AsignaImple findAsignaImple(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AsignaImple.class, id);
        } finally {
            em.close();
        }
    }

    public int getAsignaImpleCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AsignaImple> rt = cq.from(AsignaImple.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
