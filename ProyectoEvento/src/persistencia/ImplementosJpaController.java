/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Usuario;
import modelo.AsignaImple;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.Implementos;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class ImplementosJpaController implements Serializable {

    public ImplementosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Implementos implementos) {
        if (implementos.getAsignaImpleList() == null) {
            implementos.setAsignaImpleList(new ArrayList<AsignaImple>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario impAlmacenista = implementos.getImpAlmacenista();
            if (impAlmacenista != null) {
                impAlmacenista = em.getReference(impAlmacenista.getClass(), impAlmacenista.getUsuCedula());
                implementos.setImpAlmacenista(impAlmacenista);
            }
            List<AsignaImple> attachedAsignaImpleList = new ArrayList<AsignaImple>();
            for (AsignaImple asignaImpleListAsignaImpleToAttach : implementos.getAsignaImpleList()) {
                asignaImpleListAsignaImpleToAttach = em.getReference(asignaImpleListAsignaImpleToAttach.getClass(), asignaImpleListAsignaImpleToAttach.getEvImpleId());
                attachedAsignaImpleList.add(asignaImpleListAsignaImpleToAttach);
            }
            implementos.setAsignaImpleList(attachedAsignaImpleList);
            em.persist(implementos);
            if (impAlmacenista != null) {
                impAlmacenista.getImplementosList().add(implementos);
                impAlmacenista = em.merge(impAlmacenista);
            }
            for (AsignaImple asignaImpleListAsignaImple : implementos.getAsignaImpleList()) {
                Implementos oldEvImpleImpIdOfAsignaImpleListAsignaImple = asignaImpleListAsignaImple.getEvImpleImpId();
                asignaImpleListAsignaImple.setEvImpleImpId(implementos);
                asignaImpleListAsignaImple = em.merge(asignaImpleListAsignaImple);
                if (oldEvImpleImpIdOfAsignaImpleListAsignaImple != null) {
                    oldEvImpleImpIdOfAsignaImpleListAsignaImple.getAsignaImpleList().remove(asignaImpleListAsignaImple);
                    oldEvImpleImpIdOfAsignaImpleListAsignaImple = em.merge(oldEvImpleImpIdOfAsignaImpleListAsignaImple);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Implementos implementos) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Implementos persistentImplementos = em.find(Implementos.class, implementos.getImpId());
            Usuario impAlmacenistaOld = persistentImplementos.getImpAlmacenista();
            Usuario impAlmacenistaNew = implementos.getImpAlmacenista();
            List<AsignaImple> asignaImpleListOld = persistentImplementos.getAsignaImpleList();
            List<AsignaImple> asignaImpleListNew = implementos.getAsignaImpleList();
            List<String> illegalOrphanMessages = null;
            for (AsignaImple asignaImpleListOldAsignaImple : asignaImpleListOld) {
                if (!asignaImpleListNew.contains(asignaImpleListOldAsignaImple)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AsignaImple " + asignaImpleListOldAsignaImple + " since its evImpleImpId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (impAlmacenistaNew != null) {
                impAlmacenistaNew = em.getReference(impAlmacenistaNew.getClass(), impAlmacenistaNew.getUsuCedula());
                implementos.setImpAlmacenista(impAlmacenistaNew);
            }
            List<AsignaImple> attachedAsignaImpleListNew = new ArrayList<AsignaImple>();
            for (AsignaImple asignaImpleListNewAsignaImpleToAttach : asignaImpleListNew) {
                asignaImpleListNewAsignaImpleToAttach = em.getReference(asignaImpleListNewAsignaImpleToAttach.getClass(), asignaImpleListNewAsignaImpleToAttach.getEvImpleId());
                attachedAsignaImpleListNew.add(asignaImpleListNewAsignaImpleToAttach);
            }
            asignaImpleListNew = attachedAsignaImpleListNew;
            implementos.setAsignaImpleList(asignaImpleListNew);
            implementos = em.merge(implementos);
            if (impAlmacenistaOld != null && !impAlmacenistaOld.equals(impAlmacenistaNew)) {
                impAlmacenistaOld.getImplementosList().remove(implementos);
                impAlmacenistaOld = em.merge(impAlmacenistaOld);
            }
            if (impAlmacenistaNew != null && !impAlmacenistaNew.equals(impAlmacenistaOld)) {
                impAlmacenistaNew.getImplementosList().add(implementos);
                impAlmacenistaNew = em.merge(impAlmacenistaNew);
            }
            for (AsignaImple asignaImpleListNewAsignaImple : asignaImpleListNew) {
                if (!asignaImpleListOld.contains(asignaImpleListNewAsignaImple)) {
                    Implementos oldEvImpleImpIdOfAsignaImpleListNewAsignaImple = asignaImpleListNewAsignaImple.getEvImpleImpId();
                    asignaImpleListNewAsignaImple.setEvImpleImpId(implementos);
                    asignaImpleListNewAsignaImple = em.merge(asignaImpleListNewAsignaImple);
                    if (oldEvImpleImpIdOfAsignaImpleListNewAsignaImple != null && !oldEvImpleImpIdOfAsignaImpleListNewAsignaImple.equals(implementos)) {
                        oldEvImpleImpIdOfAsignaImpleListNewAsignaImple.getAsignaImpleList().remove(asignaImpleListNewAsignaImple);
                        oldEvImpleImpIdOfAsignaImpleListNewAsignaImple = em.merge(oldEvImpleImpIdOfAsignaImpleListNewAsignaImple);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = implementos.getImpId();
                if (findImplementos(id) == null) {
                    throw new NonexistentEntityException("The implementos with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Implementos implementos;
            try {
                implementos = em.getReference(Implementos.class, id);
                implementos.getImpId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The implementos with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<AsignaImple> asignaImpleListOrphanCheck = implementos.getAsignaImpleList();
            for (AsignaImple asignaImpleListOrphanCheckAsignaImple : asignaImpleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Implementos (" + implementos + ") cannot be destroyed since the AsignaImple " + asignaImpleListOrphanCheckAsignaImple + " in its asignaImpleList field has a non-nullable evImpleImpId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Usuario impAlmacenista = implementos.getImpAlmacenista();
            if (impAlmacenista != null) {
                impAlmacenista.getImplementosList().remove(implementos);
                impAlmacenista = em.merge(impAlmacenista);
            }
            em.remove(implementos);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Implementos> findImplementosEntities() {
        return findImplementosEntities(true, -1, -1);
    }

    public List<Implementos> findImplementosEntities(int maxResults, int firstResult) {
        return findImplementosEntities(false, maxResults, firstResult);
    }

    private List<Implementos> findImplementosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Implementos.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Implementos findImplementos(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Implementos.class, id);
        } finally {
            em.close();
        }
    }

    public int getImplementosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Implementos> rt = cq.from(Implementos.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
