/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.AsignaEspacio;
import modelo.Espacios;
import modelo.Evento;
import modelo.Usuario;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class AsignaEspacioJpaController implements Serializable {

    public AsignaEspacioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(AsignaEspacio asignaEspacio) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Espacios evEspEspId = asignaEspacio.getEvEspEspId();
            if (evEspEspId != null) {
                evEspEspId = em.getReference(evEspEspId.getClass(), evEspEspId.getEspId());
                asignaEspacio.setEvEspEspId(evEspEspId);
            }
            Evento evEspEvId = asignaEspacio.getEvEspEvId();
            if (evEspEvId != null) {
                evEspEvId = em.getReference(evEspEvId.getClass(), evEspEvId.getEvId());
                asignaEspacio.setEvEspEvId(evEspEvId);
            }
            Usuario evEspAdmin = asignaEspacio.getEvEspAdmin();
            if (evEspAdmin != null) {
                evEspAdmin = em.getReference(evEspAdmin.getClass(), evEspAdmin.getUsuCedula());
                asignaEspacio.setEvEspAdmin(evEspAdmin);
            }
            em.persist(asignaEspacio);
            if (evEspEspId != null) {
                evEspEspId.getAsignaEspacioList().add(asignaEspacio);
                evEspEspId = em.merge(evEspEspId);
            }
            if (evEspEvId != null) {
                evEspEvId.getAsignaEspacioList().add(asignaEspacio);
                evEspEvId = em.merge(evEspEvId);
            }
            if (evEspAdmin != null) {
                evEspAdmin.getAsignaEspacioList().add(asignaEspacio);
                evEspAdmin = em.merge(evEspAdmin);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(AsignaEspacio asignaEspacio) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AsignaEspacio persistentAsignaEspacio = em.find(AsignaEspacio.class, asignaEspacio.getEvEspId());
            Espacios evEspEspIdOld = persistentAsignaEspacio.getEvEspEspId();
            Espacios evEspEspIdNew = asignaEspacio.getEvEspEspId();
            Evento evEspEvIdOld = persistentAsignaEspacio.getEvEspEvId();
            Evento evEspEvIdNew = asignaEspacio.getEvEspEvId();
            Usuario evEspAdminOld = persistentAsignaEspacio.getEvEspAdmin();
            Usuario evEspAdminNew = asignaEspacio.getEvEspAdmin();
            if (evEspEspIdNew != null) {
                evEspEspIdNew = em.getReference(evEspEspIdNew.getClass(), evEspEspIdNew.getEspId());
                asignaEspacio.setEvEspEspId(evEspEspIdNew);
            }
            if (evEspEvIdNew != null) {
                evEspEvIdNew = em.getReference(evEspEvIdNew.getClass(), evEspEvIdNew.getEvId());
                asignaEspacio.setEvEspEvId(evEspEvIdNew);
            }
            if (evEspAdminNew != null) {
                evEspAdminNew = em.getReference(evEspAdminNew.getClass(), evEspAdminNew.getUsuCedula());
                asignaEspacio.setEvEspAdmin(evEspAdminNew);
            }
            asignaEspacio = em.merge(asignaEspacio);
            if (evEspEspIdOld != null && !evEspEspIdOld.equals(evEspEspIdNew)) {
                evEspEspIdOld.getAsignaEspacioList().remove(asignaEspacio);
                evEspEspIdOld = em.merge(evEspEspIdOld);
            }
            if (evEspEspIdNew != null && !evEspEspIdNew.equals(evEspEspIdOld)) {
                evEspEspIdNew.getAsignaEspacioList().add(asignaEspacio);
                evEspEspIdNew = em.merge(evEspEspIdNew);
            }
            if (evEspEvIdOld != null && !evEspEvIdOld.equals(evEspEvIdNew)) {
                evEspEvIdOld.getAsignaEspacioList().remove(asignaEspacio);
                evEspEvIdOld = em.merge(evEspEvIdOld);
            }
            if (evEspEvIdNew != null && !evEspEvIdNew.equals(evEspEvIdOld)) {
                evEspEvIdNew.getAsignaEspacioList().add(asignaEspacio);
                evEspEvIdNew = em.merge(evEspEvIdNew);
            }
            if (evEspAdminOld != null && !evEspAdminOld.equals(evEspAdminNew)) {
                evEspAdminOld.getAsignaEspacioList().remove(asignaEspacio);
                evEspAdminOld = em.merge(evEspAdminOld);
            }
            if (evEspAdminNew != null && !evEspAdminNew.equals(evEspAdminOld)) {
                evEspAdminNew.getAsignaEspacioList().add(asignaEspacio);
                evEspAdminNew = em.merge(evEspAdminNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = asignaEspacio.getEvEspId();
                if (findAsignaEspacio(id) == null) {
                    throw new NonexistentEntityException("The asignaEspacio with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            AsignaEspacio asignaEspacio;
            try {
                asignaEspacio = em.getReference(AsignaEspacio.class, id);
                asignaEspacio.getEvEspId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The asignaEspacio with id " + id + " no longer exists.", enfe);
            }
            Espacios evEspEspId = asignaEspacio.getEvEspEspId();
            if (evEspEspId != null) {
                evEspEspId.getAsignaEspacioList().remove(asignaEspacio);
                evEspEspId = em.merge(evEspEspId);
            }
            Evento evEspEvId = asignaEspacio.getEvEspEvId();
            if (evEspEvId != null) {
                evEspEvId.getAsignaEspacioList().remove(asignaEspacio);
                evEspEvId = em.merge(evEspEvId);
            }
            Usuario evEspAdmin = asignaEspacio.getEvEspAdmin();
            if (evEspAdmin != null) {
                evEspAdmin.getAsignaEspacioList().remove(asignaEspacio);
                evEspAdmin = em.merge(evEspAdmin);
            }
            em.remove(asignaEspacio);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<AsignaEspacio> findAsignaEspacioEntities() {
        return findAsignaEspacioEntities(true, -1, -1);
    }

    public List<AsignaEspacio> findAsignaEspacioEntities(int maxResults, int firstResult) {
        return findAsignaEspacioEntities(false, maxResults, firstResult);
    }

    private List<AsignaEspacio> findAsignaEspacioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(AsignaEspacio.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public AsignaEspacio findAsignaEspacio(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(AsignaEspacio.class, id);
        } finally {
            em.close();
        }
    }

    public int getAsignaEspacioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<AsignaEspacio> rt = cq.from(AsignaEspacio.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
