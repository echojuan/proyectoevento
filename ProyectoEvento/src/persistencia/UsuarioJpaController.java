/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.General;
import modelo.AsignaImple;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.AsignaEspacio;
import modelo.Encuesta;
import modelo.Incripcion;
import modelo.Implementos;
import modelo.Espacios;
import modelo.Usuario;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;
import persistencia.exceptions.PreexistingEntityException;

/**
 *
 * @author Usuario
 */
public class UsuarioJpaController implements Serializable {

    public UsuarioJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Usuario usuario) throws PreexistingEntityException, Exception {
        if (usuario.getAsignaImpleList() == null) {
            usuario.setAsignaImpleList(new ArrayList<AsignaImple>());
        }
        if (usuario.getAsignaEspacioList() == null) {
            usuario.setAsignaEspacioList(new ArrayList<AsignaEspacio>());
        }
        if (usuario.getEncuestaList() == null) {
            usuario.setEncuestaList(new ArrayList<Encuesta>());
        }
        if (usuario.getIncripcionList() == null) {
            usuario.setIncripcionList(new ArrayList<Incripcion>());
        }
        if (usuario.getImplementosList() == null) {
            usuario.setImplementosList(new ArrayList<Implementos>());
        }
        if (usuario.getEspaciosList() == null) {
            usuario.setEspaciosList(new ArrayList<Espacios>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            General usuTipoGenId = usuario.getUsuTipoGenId();
            if (usuTipoGenId != null) {
                usuTipoGenId = em.getReference(usuTipoGenId.getClass(), usuTipoGenId.getGenId());
                usuario.setUsuTipoGenId(usuTipoGenId);
            }
            List<AsignaImple> attachedAsignaImpleList = new ArrayList<AsignaImple>();
            for (AsignaImple asignaImpleListAsignaImpleToAttach : usuario.getAsignaImpleList()) {
                asignaImpleListAsignaImpleToAttach = em.getReference(asignaImpleListAsignaImpleToAttach.getClass(), asignaImpleListAsignaImpleToAttach.getEvImpleId());
                attachedAsignaImpleList.add(asignaImpleListAsignaImpleToAttach);
            }
            usuario.setAsignaImpleList(attachedAsignaImpleList);
            List<AsignaEspacio> attachedAsignaEspacioList = new ArrayList<AsignaEspacio>();
            for (AsignaEspacio asignaEspacioListAsignaEspacioToAttach : usuario.getAsignaEspacioList()) {
                asignaEspacioListAsignaEspacioToAttach = em.getReference(asignaEspacioListAsignaEspacioToAttach.getClass(), asignaEspacioListAsignaEspacioToAttach.getEvEspId());
                attachedAsignaEspacioList.add(asignaEspacioListAsignaEspacioToAttach);
            }
            usuario.setAsignaEspacioList(attachedAsignaEspacioList);
            List<Encuesta> attachedEncuestaList = new ArrayList<Encuesta>();
            for (Encuesta encuestaListEncuestaToAttach : usuario.getEncuestaList()) {
                encuestaListEncuestaToAttach = em.getReference(encuestaListEncuestaToAttach.getClass(), encuestaListEncuestaToAttach.getEncId());
                attachedEncuestaList.add(encuestaListEncuestaToAttach);
            }
            usuario.setEncuestaList(attachedEncuestaList);
            List<Incripcion> attachedIncripcionList = new ArrayList<Incripcion>();
            for (Incripcion incripcionListIncripcionToAttach : usuario.getIncripcionList()) {
                incripcionListIncripcionToAttach = em.getReference(incripcionListIncripcionToAttach.getClass(), incripcionListIncripcionToAttach.getIncriId());
                attachedIncripcionList.add(incripcionListIncripcionToAttach);
            }
            usuario.setIncripcionList(attachedIncripcionList);
            List<Implementos> attachedImplementosList = new ArrayList<Implementos>();
            for (Implementos implementosListImplementosToAttach : usuario.getImplementosList()) {
                implementosListImplementosToAttach = em.getReference(implementosListImplementosToAttach.getClass(), implementosListImplementosToAttach.getImpId());
                attachedImplementosList.add(implementosListImplementosToAttach);
            }
            usuario.setImplementosList(attachedImplementosList);
            List<Espacios> attachedEspaciosList = new ArrayList<Espacios>();
            for (Espacios espaciosListEspaciosToAttach : usuario.getEspaciosList()) {
                espaciosListEspaciosToAttach = em.getReference(espaciosListEspaciosToAttach.getClass(), espaciosListEspaciosToAttach.getEspId());
                attachedEspaciosList.add(espaciosListEspaciosToAttach);
            }
            usuario.setEspaciosList(attachedEspaciosList);
            em.persist(usuario);
            if (usuTipoGenId != null) {
                usuTipoGenId.getUsuarioList().add(usuario);
                usuTipoGenId = em.merge(usuTipoGenId);
            }
            for (AsignaImple asignaImpleListAsignaImple : usuario.getAsignaImpleList()) {
                Usuario oldEvImpleAdminOfAsignaImpleListAsignaImple = asignaImpleListAsignaImple.getEvImpleAdmin();
                asignaImpleListAsignaImple.setEvImpleAdmin(usuario);
                asignaImpleListAsignaImple = em.merge(asignaImpleListAsignaImple);
                if (oldEvImpleAdminOfAsignaImpleListAsignaImple != null) {
                    oldEvImpleAdminOfAsignaImpleListAsignaImple.getAsignaImpleList().remove(asignaImpleListAsignaImple);
                    oldEvImpleAdminOfAsignaImpleListAsignaImple = em.merge(oldEvImpleAdminOfAsignaImpleListAsignaImple);
                }
            }
            for (AsignaEspacio asignaEspacioListAsignaEspacio : usuario.getAsignaEspacioList()) {
                Usuario oldEvEspAdminOfAsignaEspacioListAsignaEspacio = asignaEspacioListAsignaEspacio.getEvEspAdmin();
                asignaEspacioListAsignaEspacio.setEvEspAdmin(usuario);
                asignaEspacioListAsignaEspacio = em.merge(asignaEspacioListAsignaEspacio);
                if (oldEvEspAdminOfAsignaEspacioListAsignaEspacio != null) {
                    oldEvEspAdminOfAsignaEspacioListAsignaEspacio.getAsignaEspacioList().remove(asignaEspacioListAsignaEspacio);
                    oldEvEspAdminOfAsignaEspacioListAsignaEspacio = em.merge(oldEvEspAdminOfAsignaEspacioListAsignaEspacio);
                }
            }
            for (Encuesta encuestaListEncuesta : usuario.getEncuestaList()) {
                Usuario oldEncUsuCedulaOfEncuestaListEncuesta = encuestaListEncuesta.getEncUsuCedula();
                encuestaListEncuesta.setEncUsuCedula(usuario);
                encuestaListEncuesta = em.merge(encuestaListEncuesta);
                if (oldEncUsuCedulaOfEncuestaListEncuesta != null) {
                    oldEncUsuCedulaOfEncuestaListEncuesta.getEncuestaList().remove(encuestaListEncuesta);
                    oldEncUsuCedulaOfEncuestaListEncuesta = em.merge(oldEncUsuCedulaOfEncuestaListEncuesta);
                }
            }
            for (Incripcion incripcionListIncripcion : usuario.getIncripcionList()) {
                Usuario oldEvUsuUsuCedulaOfIncripcionListIncripcion = incripcionListIncripcion.getEvUsuUsuCedula();
                incripcionListIncripcion.setEvUsuUsuCedula(usuario);
                incripcionListIncripcion = em.merge(incripcionListIncripcion);
                if (oldEvUsuUsuCedulaOfIncripcionListIncripcion != null) {
                    oldEvUsuUsuCedulaOfIncripcionListIncripcion.getIncripcionList().remove(incripcionListIncripcion);
                    oldEvUsuUsuCedulaOfIncripcionListIncripcion = em.merge(oldEvUsuUsuCedulaOfIncripcionListIncripcion);
                }
            }
            for (Implementos implementosListImplementos : usuario.getImplementosList()) {
                Usuario oldImpAlmacenistaOfImplementosListImplementos = implementosListImplementos.getImpAlmacenista();
                implementosListImplementos.setImpAlmacenista(usuario);
                implementosListImplementos = em.merge(implementosListImplementos);
                if (oldImpAlmacenistaOfImplementosListImplementos != null) {
                    oldImpAlmacenistaOfImplementosListImplementos.getImplementosList().remove(implementosListImplementos);
                    oldImpAlmacenistaOfImplementosListImplementos = em.merge(oldImpAlmacenistaOfImplementosListImplementos);
                }
            }
            for (Espacios espaciosListEspacios : usuario.getEspaciosList()) {
                Usuario oldEspAdminOfEspaciosListEspacios = espaciosListEspacios.getEspAdmin();
                espaciosListEspacios.setEspAdmin(usuario);
                espaciosListEspacios = em.merge(espaciosListEspacios);
                if (oldEspAdminOfEspaciosListEspacios != null) {
                    oldEspAdminOfEspaciosListEspacios.getEspaciosList().remove(espaciosListEspacios);
                    oldEspAdminOfEspaciosListEspacios = em.merge(oldEspAdminOfEspaciosListEspacios);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findUsuario(usuario.getUsuCedula()) != null) {
                throw new PreexistingEntityException("Usuario " + usuario + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Usuario usuario) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario persistentUsuario = em.find(Usuario.class, usuario.getUsuCedula());
            General usuTipoGenIdOld = persistentUsuario.getUsuTipoGenId();
            General usuTipoGenIdNew = usuario.getUsuTipoGenId();
            List<AsignaImple> asignaImpleListOld = persistentUsuario.getAsignaImpleList();
            List<AsignaImple> asignaImpleListNew = usuario.getAsignaImpleList();
            List<AsignaEspacio> asignaEspacioListOld = persistentUsuario.getAsignaEspacioList();
            List<AsignaEspacio> asignaEspacioListNew = usuario.getAsignaEspacioList();
            List<Encuesta> encuestaListOld = persistentUsuario.getEncuestaList();
            List<Encuesta> encuestaListNew = usuario.getEncuestaList();
            List<Incripcion> incripcionListOld = persistentUsuario.getIncripcionList();
            List<Incripcion> incripcionListNew = usuario.getIncripcionList();
            List<Implementos> implementosListOld = persistentUsuario.getImplementosList();
            List<Implementos> implementosListNew = usuario.getImplementosList();
            List<Espacios> espaciosListOld = persistentUsuario.getEspaciosList();
            List<Espacios> espaciosListNew = usuario.getEspaciosList();
            List<String> illegalOrphanMessages = null;
            for (AsignaImple asignaImpleListOldAsignaImple : asignaImpleListOld) {
                if (!asignaImpleListNew.contains(asignaImpleListOldAsignaImple)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AsignaImple " + asignaImpleListOldAsignaImple + " since its evImpleAdmin field is not nullable.");
                }
            }
            for (AsignaEspacio asignaEspacioListOldAsignaEspacio : asignaEspacioListOld) {
                if (!asignaEspacioListNew.contains(asignaEspacioListOldAsignaEspacio)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AsignaEspacio " + asignaEspacioListOldAsignaEspacio + " since its evEspAdmin field is not nullable.");
                }
            }
            for (Encuesta encuestaListOldEncuesta : encuestaListOld) {
                if (!encuestaListNew.contains(encuestaListOldEncuesta)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Encuesta " + encuestaListOldEncuesta + " since its encUsuCedula field is not nullable.");
                }
            }
            for (Incripcion incripcionListOldIncripcion : incripcionListOld) {
                if (!incripcionListNew.contains(incripcionListOldIncripcion)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Incripcion " + incripcionListOldIncripcion + " since its evUsuUsuCedula field is not nullable.");
                }
            }
            for (Implementos implementosListOldImplementos : implementosListOld) {
                if (!implementosListNew.contains(implementosListOldImplementos)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Implementos " + implementosListOldImplementos + " since its impAlmacenista field is not nullable.");
                }
            }
            for (Espacios espaciosListOldEspacios : espaciosListOld) {
                if (!espaciosListNew.contains(espaciosListOldEspacios)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Espacios " + espaciosListOldEspacios + " since its espAdmin field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (usuTipoGenIdNew != null) {
                usuTipoGenIdNew = em.getReference(usuTipoGenIdNew.getClass(), usuTipoGenIdNew.getGenId());
                usuario.setUsuTipoGenId(usuTipoGenIdNew);
            }
            List<AsignaImple> attachedAsignaImpleListNew = new ArrayList<AsignaImple>();
            for (AsignaImple asignaImpleListNewAsignaImpleToAttach : asignaImpleListNew) {
                asignaImpleListNewAsignaImpleToAttach = em.getReference(asignaImpleListNewAsignaImpleToAttach.getClass(), asignaImpleListNewAsignaImpleToAttach.getEvImpleId());
                attachedAsignaImpleListNew.add(asignaImpleListNewAsignaImpleToAttach);
            }
            asignaImpleListNew = attachedAsignaImpleListNew;
            usuario.setAsignaImpleList(asignaImpleListNew);
            List<AsignaEspacio> attachedAsignaEspacioListNew = new ArrayList<AsignaEspacio>();
            for (AsignaEspacio asignaEspacioListNewAsignaEspacioToAttach : asignaEspacioListNew) {
                asignaEspacioListNewAsignaEspacioToAttach = em.getReference(asignaEspacioListNewAsignaEspacioToAttach.getClass(), asignaEspacioListNewAsignaEspacioToAttach.getEvEspId());
                attachedAsignaEspacioListNew.add(asignaEspacioListNewAsignaEspacioToAttach);
            }
            asignaEspacioListNew = attachedAsignaEspacioListNew;
            usuario.setAsignaEspacioList(asignaEspacioListNew);
            List<Encuesta> attachedEncuestaListNew = new ArrayList<Encuesta>();
            for (Encuesta encuestaListNewEncuestaToAttach : encuestaListNew) {
                encuestaListNewEncuestaToAttach = em.getReference(encuestaListNewEncuestaToAttach.getClass(), encuestaListNewEncuestaToAttach.getEncId());
                attachedEncuestaListNew.add(encuestaListNewEncuestaToAttach);
            }
            encuestaListNew = attachedEncuestaListNew;
            usuario.setEncuestaList(encuestaListNew);
            List<Incripcion> attachedIncripcionListNew = new ArrayList<Incripcion>();
            for (Incripcion incripcionListNewIncripcionToAttach : incripcionListNew) {
                incripcionListNewIncripcionToAttach = em.getReference(incripcionListNewIncripcionToAttach.getClass(), incripcionListNewIncripcionToAttach.getIncriId());
                attachedIncripcionListNew.add(incripcionListNewIncripcionToAttach);
            }
            incripcionListNew = attachedIncripcionListNew;
            usuario.setIncripcionList(incripcionListNew);
            List<Implementos> attachedImplementosListNew = new ArrayList<Implementos>();
            for (Implementos implementosListNewImplementosToAttach : implementosListNew) {
                implementosListNewImplementosToAttach = em.getReference(implementosListNewImplementosToAttach.getClass(), implementosListNewImplementosToAttach.getImpId());
                attachedImplementosListNew.add(implementosListNewImplementosToAttach);
            }
            implementosListNew = attachedImplementosListNew;
            usuario.setImplementosList(implementosListNew);
            List<Espacios> attachedEspaciosListNew = new ArrayList<Espacios>();
            for (Espacios espaciosListNewEspaciosToAttach : espaciosListNew) {
                espaciosListNewEspaciosToAttach = em.getReference(espaciosListNewEspaciosToAttach.getClass(), espaciosListNewEspaciosToAttach.getEspId());
                attachedEspaciosListNew.add(espaciosListNewEspaciosToAttach);
            }
            espaciosListNew = attachedEspaciosListNew;
            usuario.setEspaciosList(espaciosListNew);
            usuario = em.merge(usuario);
            if (usuTipoGenIdOld != null && !usuTipoGenIdOld.equals(usuTipoGenIdNew)) {
                usuTipoGenIdOld.getUsuarioList().remove(usuario);
                usuTipoGenIdOld = em.merge(usuTipoGenIdOld);
            }
            if (usuTipoGenIdNew != null && !usuTipoGenIdNew.equals(usuTipoGenIdOld)) {
                usuTipoGenIdNew.getUsuarioList().add(usuario);
                usuTipoGenIdNew = em.merge(usuTipoGenIdNew);
            }
            for (AsignaImple asignaImpleListNewAsignaImple : asignaImpleListNew) {
                if (!asignaImpleListOld.contains(asignaImpleListNewAsignaImple)) {
                    Usuario oldEvImpleAdminOfAsignaImpleListNewAsignaImple = asignaImpleListNewAsignaImple.getEvImpleAdmin();
                    asignaImpleListNewAsignaImple.setEvImpleAdmin(usuario);
                    asignaImpleListNewAsignaImple = em.merge(asignaImpleListNewAsignaImple);
                    if (oldEvImpleAdminOfAsignaImpleListNewAsignaImple != null && !oldEvImpleAdminOfAsignaImpleListNewAsignaImple.equals(usuario)) {
                        oldEvImpleAdminOfAsignaImpleListNewAsignaImple.getAsignaImpleList().remove(asignaImpleListNewAsignaImple);
                        oldEvImpleAdminOfAsignaImpleListNewAsignaImple = em.merge(oldEvImpleAdminOfAsignaImpleListNewAsignaImple);
                    }
                }
            }
            for (AsignaEspacio asignaEspacioListNewAsignaEspacio : asignaEspacioListNew) {
                if (!asignaEspacioListOld.contains(asignaEspacioListNewAsignaEspacio)) {
                    Usuario oldEvEspAdminOfAsignaEspacioListNewAsignaEspacio = asignaEspacioListNewAsignaEspacio.getEvEspAdmin();
                    asignaEspacioListNewAsignaEspacio.setEvEspAdmin(usuario);
                    asignaEspacioListNewAsignaEspacio = em.merge(asignaEspacioListNewAsignaEspacio);
                    if (oldEvEspAdminOfAsignaEspacioListNewAsignaEspacio != null && !oldEvEspAdminOfAsignaEspacioListNewAsignaEspacio.equals(usuario)) {
                        oldEvEspAdminOfAsignaEspacioListNewAsignaEspacio.getAsignaEspacioList().remove(asignaEspacioListNewAsignaEspacio);
                        oldEvEspAdminOfAsignaEspacioListNewAsignaEspacio = em.merge(oldEvEspAdminOfAsignaEspacioListNewAsignaEspacio);
                    }
                }
            }
            for (Encuesta encuestaListNewEncuesta : encuestaListNew) {
                if (!encuestaListOld.contains(encuestaListNewEncuesta)) {
                    Usuario oldEncUsuCedulaOfEncuestaListNewEncuesta = encuestaListNewEncuesta.getEncUsuCedula();
                    encuestaListNewEncuesta.setEncUsuCedula(usuario);
                    encuestaListNewEncuesta = em.merge(encuestaListNewEncuesta);
                    if (oldEncUsuCedulaOfEncuestaListNewEncuesta != null && !oldEncUsuCedulaOfEncuestaListNewEncuesta.equals(usuario)) {
                        oldEncUsuCedulaOfEncuestaListNewEncuesta.getEncuestaList().remove(encuestaListNewEncuesta);
                        oldEncUsuCedulaOfEncuestaListNewEncuesta = em.merge(oldEncUsuCedulaOfEncuestaListNewEncuesta);
                    }
                }
            }
            for (Incripcion incripcionListNewIncripcion : incripcionListNew) {
                if (!incripcionListOld.contains(incripcionListNewIncripcion)) {
                    Usuario oldEvUsuUsuCedulaOfIncripcionListNewIncripcion = incripcionListNewIncripcion.getEvUsuUsuCedula();
                    incripcionListNewIncripcion.setEvUsuUsuCedula(usuario);
                    incripcionListNewIncripcion = em.merge(incripcionListNewIncripcion);
                    if (oldEvUsuUsuCedulaOfIncripcionListNewIncripcion != null && !oldEvUsuUsuCedulaOfIncripcionListNewIncripcion.equals(usuario)) {
                        oldEvUsuUsuCedulaOfIncripcionListNewIncripcion.getIncripcionList().remove(incripcionListNewIncripcion);
                        oldEvUsuUsuCedulaOfIncripcionListNewIncripcion = em.merge(oldEvUsuUsuCedulaOfIncripcionListNewIncripcion);
                    }
                }
            }
            for (Implementos implementosListNewImplementos : implementosListNew) {
                if (!implementosListOld.contains(implementosListNewImplementos)) {
                    Usuario oldImpAlmacenistaOfImplementosListNewImplementos = implementosListNewImplementos.getImpAlmacenista();
                    implementosListNewImplementos.setImpAlmacenista(usuario);
                    implementosListNewImplementos = em.merge(implementosListNewImplementos);
                    if (oldImpAlmacenistaOfImplementosListNewImplementos != null && !oldImpAlmacenistaOfImplementosListNewImplementos.equals(usuario)) {
                        oldImpAlmacenistaOfImplementosListNewImplementos.getImplementosList().remove(implementosListNewImplementos);
                        oldImpAlmacenistaOfImplementosListNewImplementos = em.merge(oldImpAlmacenistaOfImplementosListNewImplementos);
                    }
                }
            }
            for (Espacios espaciosListNewEspacios : espaciosListNew) {
                if (!espaciosListOld.contains(espaciosListNewEspacios)) {
                    Usuario oldEspAdminOfEspaciosListNewEspacios = espaciosListNewEspacios.getEspAdmin();
                    espaciosListNewEspacios.setEspAdmin(usuario);
                    espaciosListNewEspacios = em.merge(espaciosListNewEspacios);
                    if (oldEspAdminOfEspaciosListNewEspacios != null && !oldEspAdminOfEspaciosListNewEspacios.equals(usuario)) {
                        oldEspAdminOfEspaciosListNewEspacios.getEspaciosList().remove(espaciosListNewEspacios);
                        oldEspAdminOfEspaciosListNewEspacios = em.merge(oldEspAdminOfEspaciosListNewEspacios);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = usuario.getUsuCedula();
                if (findUsuario(id) == null) {
                    throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuario;
            try {
                usuario = em.getReference(Usuario.class, id);
                usuario.getUsuCedula();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The usuario with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<AsignaImple> asignaImpleListOrphanCheck = usuario.getAsignaImpleList();
            for (AsignaImple asignaImpleListOrphanCheckAsignaImple : asignaImpleListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the AsignaImple " + asignaImpleListOrphanCheckAsignaImple + " in its asignaImpleList field has a non-nullable evImpleAdmin field.");
            }
            List<AsignaEspacio> asignaEspacioListOrphanCheck = usuario.getAsignaEspacioList();
            for (AsignaEspacio asignaEspacioListOrphanCheckAsignaEspacio : asignaEspacioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the AsignaEspacio " + asignaEspacioListOrphanCheckAsignaEspacio + " in its asignaEspacioList field has a non-nullable evEspAdmin field.");
            }
            List<Encuesta> encuestaListOrphanCheck = usuario.getEncuestaList();
            for (Encuesta encuestaListOrphanCheckEncuesta : encuestaListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Encuesta " + encuestaListOrphanCheckEncuesta + " in its encuestaList field has a non-nullable encUsuCedula field.");
            }
            List<Incripcion> incripcionListOrphanCheck = usuario.getIncripcionList();
            for (Incripcion incripcionListOrphanCheckIncripcion : incripcionListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Incripcion " + incripcionListOrphanCheckIncripcion + " in its incripcionList field has a non-nullable evUsuUsuCedula field.");
            }
            List<Implementos> implementosListOrphanCheck = usuario.getImplementosList();
            for (Implementos implementosListOrphanCheckImplementos : implementosListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Implementos " + implementosListOrphanCheckImplementos + " in its implementosList field has a non-nullable impAlmacenista field.");
            }
            List<Espacios> espaciosListOrphanCheck = usuario.getEspaciosList();
            for (Espacios espaciosListOrphanCheckEspacios : espaciosListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Usuario (" + usuario + ") cannot be destroyed since the Espacios " + espaciosListOrphanCheckEspacios + " in its espaciosList field has a non-nullable espAdmin field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            General usuTipoGenId = usuario.getUsuTipoGenId();
            if (usuTipoGenId != null) {
                usuTipoGenId.getUsuarioList().remove(usuario);
                usuTipoGenId = em.merge(usuTipoGenId);
            }
            em.remove(usuario);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Usuario> findUsuarioEntities() {
        return findUsuarioEntities(true, -1, -1);
    }

    public List<Usuario> findUsuarioEntities(int maxResults, int firstResult) {
        return findUsuarioEntities(false, maxResults, firstResult);
    }

    private List<Usuario> findUsuarioEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Usuario.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Usuario findUsuario(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Usuario.class, id);
        } finally {
            em.close();
        }
    }

    public int getUsuarioCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Usuario> rt = cq.from(Usuario.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
