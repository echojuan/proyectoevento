/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Evento;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import modelo.General;
import modelo.Usuario;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;
import persistencia.exceptions.PreexistingEntityException;

/**
 *
 * @author Usuario
 */
public class GeneralJpaController implements Serializable {

    public GeneralJpaController() {
        this.emf = Persistence.createEntityManagerFactory("ProyectoEventoPU");
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(General general) throws PreexistingEntityException, Exception {
        if (general.getEventoList() == null) {
            general.setEventoList(new ArrayList<Evento>());
        }
        if (general.getEventoList1() == null) {
            general.setEventoList1(new ArrayList<Evento>());
        }
        if (general.getUsuarioList() == null) {
            general.setUsuarioList(new ArrayList<Usuario>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Evento> attachedEventoList = new ArrayList<Evento>();
            for (Evento eventoListEventoToAttach : general.getEventoList()) {
                eventoListEventoToAttach = em.getReference(eventoListEventoToAttach.getClass(), eventoListEventoToAttach.getEvId());
                attachedEventoList.add(eventoListEventoToAttach);
            }
            general.setEventoList(attachedEventoList);
            List<Evento> attachedEventoList1 = new ArrayList<Evento>();
            for (Evento eventoList1EventoToAttach : general.getEventoList1()) {
                eventoList1EventoToAttach = em.getReference(eventoList1EventoToAttach.getClass(), eventoList1EventoToAttach.getEvId());
                attachedEventoList1.add(eventoList1EventoToAttach);
            }
            general.setEventoList1(attachedEventoList1);
            List<Usuario> attachedUsuarioList = new ArrayList<Usuario>();
            for (Usuario usuarioListUsuarioToAttach : general.getUsuarioList()) {
                usuarioListUsuarioToAttach = em.getReference(usuarioListUsuarioToAttach.getClass(), usuarioListUsuarioToAttach.getUsuCedula());
                attachedUsuarioList.add(usuarioListUsuarioToAttach);
            }
            general.setUsuarioList(attachedUsuarioList);
            em.persist(general);
            for (Evento eventoListEvento : general.getEventoList()) {
                General oldEvCodEveOfEventoListEvento = eventoListEvento.getEvCodEve();
                eventoListEvento.setEvCodEve(general);
                eventoListEvento = em.merge(eventoListEvento);
                if (oldEvCodEveOfEventoListEvento != null) {
                    oldEvCodEveOfEventoListEvento.getEventoList().remove(eventoListEvento);
                    oldEvCodEveOfEventoListEvento = em.merge(oldEvCodEveOfEventoListEvento);
                }
            }
            for (Evento eventoList1Evento : general.getEventoList1()) {
                General oldEvCodDepeOfEventoList1Evento = eventoList1Evento.getEvCodDepe();
                eventoList1Evento.setEvCodDepe(general);
                eventoList1Evento = em.merge(eventoList1Evento);
                if (oldEvCodDepeOfEventoList1Evento != null) {
                    oldEvCodDepeOfEventoList1Evento.getEventoList1().remove(eventoList1Evento);
                    oldEvCodDepeOfEventoList1Evento = em.merge(oldEvCodDepeOfEventoList1Evento);
                }
            }
            for (Usuario usuarioListUsuario : general.getUsuarioList()) {
                General oldUsuTipoGenIdOfUsuarioListUsuario = usuarioListUsuario.getUsuTipoGenId();
                usuarioListUsuario.setUsuTipoGenId(general);
                usuarioListUsuario = em.merge(usuarioListUsuario);
                if (oldUsuTipoGenIdOfUsuarioListUsuario != null) {
                    oldUsuTipoGenIdOfUsuarioListUsuario.getUsuarioList().remove(usuarioListUsuario);
                    oldUsuTipoGenIdOfUsuarioListUsuario = em.merge(oldUsuTipoGenIdOfUsuarioListUsuario);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findGeneral(general.getGenId()) != null) {
                throw new PreexistingEntityException("General " + general + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(General general) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            General persistentGeneral = em.find(General.class, general.getGenId());
            List<Evento> eventoListOld = persistentGeneral.getEventoList();
            List<Evento> eventoListNew = general.getEventoList();
            List<Evento> eventoList1Old = persistentGeneral.getEventoList1();
            List<Evento> eventoList1New = general.getEventoList1();
            List<Usuario> usuarioListOld = persistentGeneral.getUsuarioList();
            List<Usuario> usuarioListNew = general.getUsuarioList();
            List<String> illegalOrphanMessages = null;
            for (Evento eventoListOldEvento : eventoListOld) {
                if (!eventoListNew.contains(eventoListOldEvento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Evento " + eventoListOldEvento + " since its evCodEve field is not nullable.");
                }
            }
            for (Evento eventoList1OldEvento : eventoList1Old) {
                if (!eventoList1New.contains(eventoList1OldEvento)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Evento " + eventoList1OldEvento + " since its evCodDepe field is not nullable.");
                }
            }
            for (Usuario usuarioListOldUsuario : usuarioListOld) {
                if (!usuarioListNew.contains(usuarioListOldUsuario)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Usuario " + usuarioListOldUsuario + " since its usuTipoGenId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Evento> attachedEventoListNew = new ArrayList<Evento>();
            /*for (Evento eventoListNewEventoToAttach : eventoListNew) {
                eventoListNewEventoToAttach = em.getReference(eventoListNewEventoToAttach.getClass(), eventoListNewEventoToAttach.getEvId());
                attachedEventoListNew.add(eventoListNewEventoToAttach);
            }*/
            eventoListNew = attachedEventoListNew;
            general.setEventoList(eventoListNew);
            List<Evento> attachedEventoList1New = new ArrayList<Evento>();
            /*for (Evento eventoList1NewEventoToAttach : eventoList1New) {
                eventoList1NewEventoToAttach = em.getReference(eventoList1NewEventoToAttach.getClass(), eventoList1NewEventoToAttach.getEvId());
                attachedEventoList1New.add(eventoList1NewEventoToAttach);
            }*/
            eventoList1New = attachedEventoList1New;
            general.setEventoList1(eventoList1New);
            List<Usuario> attachedUsuarioListNew = new ArrayList<Usuario>();
            /*for (Usuario usuarioListNewUsuarioToAttach : usuarioListNew) {
                usuarioListNewUsuarioToAttach = em.getReference(usuarioListNewUsuarioToAttach.getClass(), usuarioListNewUsuarioToAttach.getUsuCedula());
                attachedUsuarioListNew.add(usuarioListNewUsuarioToAttach);
            }*/
            usuarioListNew = attachedUsuarioListNew;
            general.setUsuarioList(usuarioListNew);
            general = em.merge(general);
            
            for (Evento eventoListNewEvento : eventoListNew) {
                if (!eventoListOld.contains(eventoListNewEvento)) {
                    General oldEvCodEveOfEventoListNewEvento = eventoListNewEvento.getEvCodEve();
                    eventoListNewEvento.setEvCodEve(general);
                    eventoListNewEvento = em.merge(eventoListNewEvento);
                    if (oldEvCodEveOfEventoListNewEvento != null && !oldEvCodEveOfEventoListNewEvento.equals(general)) {
                        oldEvCodEveOfEventoListNewEvento.getEventoList().remove(eventoListNewEvento);
                        oldEvCodEveOfEventoListNewEvento = em.merge(oldEvCodEveOfEventoListNewEvento);
                    }
                }
            }
            for (Evento eventoList1NewEvento : eventoList1New) {
                if (!eventoList1Old.contains(eventoList1NewEvento)) {
                    General oldEvCodDepeOfEventoList1NewEvento = eventoList1NewEvento.getEvCodDepe();
                    eventoList1NewEvento.setEvCodDepe(general);
                    eventoList1NewEvento = em.merge(eventoList1NewEvento);
                    if (oldEvCodDepeOfEventoList1NewEvento != null && !oldEvCodDepeOfEventoList1NewEvento.equals(general)) {
                        oldEvCodDepeOfEventoList1NewEvento.getEventoList1().remove(eventoList1NewEvento);
                        oldEvCodDepeOfEventoList1NewEvento = em.merge(oldEvCodDepeOfEventoList1NewEvento);
                    }
                }
            }
            for (Usuario usuarioListNewUsuario : usuarioListNew) {
                if (!usuarioListOld.contains(usuarioListNewUsuario)) {
                    General oldUsuTipoGenIdOfUsuarioListNewUsuario = usuarioListNewUsuario.getUsuTipoGenId();
                    usuarioListNewUsuario.setUsuTipoGenId(general);
                    usuarioListNewUsuario = em.merge(usuarioListNewUsuario);
                    if (oldUsuTipoGenIdOfUsuarioListNewUsuario != null && !oldUsuTipoGenIdOfUsuarioListNewUsuario.equals(general)) {
                        oldUsuTipoGenIdOfUsuarioListNewUsuario.getUsuarioList().remove(usuarioListNewUsuario);
                        oldUsuTipoGenIdOfUsuarioListNewUsuario = em.merge(oldUsuTipoGenIdOfUsuarioListNewUsuario);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = general.getGenId();
                if (findGeneral(id) == null) {
                    throw new NonexistentEntityException("The general with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            General general;
            try {
                general = em.getReference(General.class, id);
                general.getGenId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The general with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Evento> eventoListOrphanCheck = general.getEventoList();
            for (Evento eventoListOrphanCheckEvento : eventoListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This General (" + general + ") cannot be destroyed since the Evento " + eventoListOrphanCheckEvento + " in its eventoList field has a non-nullable evCodEve field.");
            }
            List<Evento> eventoList1OrphanCheck = general.getEventoList1();
            for (Evento eventoList1OrphanCheckEvento : eventoList1OrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This General (" + general + ") cannot be destroyed since the Evento " + eventoList1OrphanCheckEvento + " in its eventoList1 field has a non-nullable evCodDepe field.");
            }
            List<Usuario> usuarioListOrphanCheck = general.getUsuarioList();
            for (Usuario usuarioListOrphanCheckUsuario : usuarioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This General (" + general + ") cannot be destroyed since the Usuario " + usuarioListOrphanCheckUsuario + " in its usuarioList field has a non-nullable usuTipoGenId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(general);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<General> findGeneralEntities() {
        return findGeneralEntities(true, -1, -1);
    }

    public List<General> findGeneralEntities(int maxResults, int firstResult) {
        return findGeneralEntities(false, maxResults, firstResult);
    }

    private List<General> findGeneralEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(General.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public General findGeneral(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(General.class, id);
        } finally {
            em.close();
        }
    }

    public int getGeneralCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<General> rt = cq.from(General.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
    public List<General> buscarCateg(String categoria){
        EntityManager em = getEntityManager();
        
        String sql = "SELECT * FROM eventos.general WHERE gen_categoria='" + categoria + "'";
        
        Query q = em.createNativeQuery(sql, General.class);
        
        List<General> listaGen = q.getResultList();
        
        return listaGen;
    }
}
