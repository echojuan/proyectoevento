/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Encuesta;
import modelo.Evento;
import modelo.Usuario;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class EncuestaJpaController implements Serializable {

    public EncuestaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Encuesta encuesta) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Evento encEvId = encuesta.getEncEvId();
            if (encEvId != null) {
                encEvId = em.getReference(encEvId.getClass(), encEvId.getEvId());
                encuesta.setEncEvId(encEvId);
            }
            Usuario encUsuCedula = encuesta.getEncUsuCedula();
            if (encUsuCedula != null) {
                encUsuCedula = em.getReference(encUsuCedula.getClass(), encUsuCedula.getUsuCedula());
                encuesta.setEncUsuCedula(encUsuCedula);
            }
            em.persist(encuesta);
            if (encEvId != null) {
                encEvId.getEncuestaList().add(encuesta);
                encEvId = em.merge(encEvId);
            }
            if (encUsuCedula != null) {
                encUsuCedula.getEncuestaList().add(encuesta);
                encUsuCedula = em.merge(encUsuCedula);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Encuesta encuesta) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Encuesta persistentEncuesta = em.find(Encuesta.class, encuesta.getEncId());
            Evento encEvIdOld = persistentEncuesta.getEncEvId();
            Evento encEvIdNew = encuesta.getEncEvId();
            Usuario encUsuCedulaOld = persistentEncuesta.getEncUsuCedula();
            Usuario encUsuCedulaNew = encuesta.getEncUsuCedula();
            if (encEvIdNew != null) {
                encEvIdNew = em.getReference(encEvIdNew.getClass(), encEvIdNew.getEvId());
                encuesta.setEncEvId(encEvIdNew);
            }
            if (encUsuCedulaNew != null) {
                encUsuCedulaNew = em.getReference(encUsuCedulaNew.getClass(), encUsuCedulaNew.getUsuCedula());
                encuesta.setEncUsuCedula(encUsuCedulaNew);
            }
            encuesta = em.merge(encuesta);
            if (encEvIdOld != null && !encEvIdOld.equals(encEvIdNew)) {
                encEvIdOld.getEncuestaList().remove(encuesta);
                encEvIdOld = em.merge(encEvIdOld);
            }
            if (encEvIdNew != null && !encEvIdNew.equals(encEvIdOld)) {
                encEvIdNew.getEncuestaList().add(encuesta);
                encEvIdNew = em.merge(encEvIdNew);
            }
            if (encUsuCedulaOld != null && !encUsuCedulaOld.equals(encUsuCedulaNew)) {
                encUsuCedulaOld.getEncuestaList().remove(encuesta);
                encUsuCedulaOld = em.merge(encUsuCedulaOld);
            }
            if (encUsuCedulaNew != null && !encUsuCedulaNew.equals(encUsuCedulaOld)) {
                encUsuCedulaNew.getEncuestaList().add(encuesta);
                encUsuCedulaNew = em.merge(encUsuCedulaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = encuesta.getEncId();
                if (findEncuesta(id) == null) {
                    throw new NonexistentEntityException("The encuesta with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Encuesta encuesta;
            try {
                encuesta = em.getReference(Encuesta.class, id);
                encuesta.getEncId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The encuesta with id " + id + " no longer exists.", enfe);
            }
            Evento encEvId = encuesta.getEncEvId();
            if (encEvId != null) {
                encEvId.getEncuestaList().remove(encuesta);
                encEvId = em.merge(encEvId);
            }
            Usuario encUsuCedula = encuesta.getEncUsuCedula();
            if (encUsuCedula != null) {
                encUsuCedula.getEncuestaList().remove(encuesta);
                encUsuCedula = em.merge(encUsuCedula);
            }
            em.remove(encuesta);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Encuesta> findEncuestaEntities() {
        return findEncuestaEntities(true, -1, -1);
    }

    public List<Encuesta> findEncuestaEntities(int maxResults, int firstResult) {
        return findEncuestaEntities(false, maxResults, firstResult);
    }

    private List<Encuesta> findEncuestaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Encuesta.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Encuesta findEncuesta(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Encuesta.class, id);
        } finally {
            em.close();
        }
    }

    public int getEncuestaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Encuesta> rt = cq.from(Encuesta.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
