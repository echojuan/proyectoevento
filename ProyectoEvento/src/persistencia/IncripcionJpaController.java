/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Conferencia;
import modelo.Incripcion;
import modelo.Usuario;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class IncripcionJpaController implements Serializable {

    public IncripcionJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Incripcion incripcion) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conferencia conferencia = incripcion.getConferencia();
            if (conferencia != null) {
                conferencia = em.getReference(conferencia.getClass(), conferencia.getConferenciaPK());
                incripcion.setConferencia(conferencia);
            }
            Usuario evUsuUsuCedula = incripcion.getEvUsuUsuCedula();
            if (evUsuUsuCedula != null) {
                evUsuUsuCedula = em.getReference(evUsuUsuCedula.getClass(), evUsuUsuCedula.getUsuCedula());
                incripcion.setEvUsuUsuCedula(evUsuUsuCedula);
            }
            em.persist(incripcion);
            if (conferencia != null) {
                conferencia.getIncripcionList().add(incripcion);
                conferencia = em.merge(conferencia);
            }
            if (evUsuUsuCedula != null) {
                evUsuUsuCedula.getIncripcionList().add(incripcion);
                evUsuUsuCedula = em.merge(evUsuUsuCedula);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Incripcion incripcion) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Incripcion persistentIncripcion = em.find(Incripcion.class, incripcion.getIncriId());
            Conferencia conferenciaOld = persistentIncripcion.getConferencia();
            Conferencia conferenciaNew = incripcion.getConferencia();
            Usuario evUsuUsuCedulaOld = persistentIncripcion.getEvUsuUsuCedula();
            Usuario evUsuUsuCedulaNew = incripcion.getEvUsuUsuCedula();
            if (conferenciaNew != null) {
                conferenciaNew = em.getReference(conferenciaNew.getClass(), conferenciaNew.getConferenciaPK());
                incripcion.setConferencia(conferenciaNew);
            }
            if (evUsuUsuCedulaNew != null) {
                evUsuUsuCedulaNew = em.getReference(evUsuUsuCedulaNew.getClass(), evUsuUsuCedulaNew.getUsuCedula());
                incripcion.setEvUsuUsuCedula(evUsuUsuCedulaNew);
            }
            incripcion = em.merge(incripcion);
            if (conferenciaOld != null && !conferenciaOld.equals(conferenciaNew)) {
                conferenciaOld.getIncripcionList().remove(incripcion);
                conferenciaOld = em.merge(conferenciaOld);
            }
            if (conferenciaNew != null && !conferenciaNew.equals(conferenciaOld)) {
                conferenciaNew.getIncripcionList().add(incripcion);
                conferenciaNew = em.merge(conferenciaNew);
            }
            if (evUsuUsuCedulaOld != null && !evUsuUsuCedulaOld.equals(evUsuUsuCedulaNew)) {
                evUsuUsuCedulaOld.getIncripcionList().remove(incripcion);
                evUsuUsuCedulaOld = em.merge(evUsuUsuCedulaOld);
            }
            if (evUsuUsuCedulaNew != null && !evUsuUsuCedulaNew.equals(evUsuUsuCedulaOld)) {
                evUsuUsuCedulaNew.getIncripcionList().add(incripcion);
                evUsuUsuCedulaNew = em.merge(evUsuUsuCedulaNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Long id = incripcion.getIncriId();
                if (findIncripcion(id) == null) {
                    throw new NonexistentEntityException("The incripcion with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Long id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Incripcion incripcion;
            try {
                incripcion = em.getReference(Incripcion.class, id);
                incripcion.getIncriId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The incripcion with id " + id + " no longer exists.", enfe);
            }
            Conferencia conferencia = incripcion.getConferencia();
            if (conferencia != null) {
                conferencia.getIncripcionList().remove(incripcion);
                conferencia = em.merge(conferencia);
            }
            Usuario evUsuUsuCedula = incripcion.getEvUsuUsuCedula();
            if (evUsuUsuCedula != null) {
                evUsuUsuCedula.getIncripcionList().remove(incripcion);
                evUsuUsuCedula = em.merge(evUsuUsuCedula);
            }
            em.remove(incripcion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Incripcion> findIncripcionEntities() {
        return findIncripcionEntities(true, -1, -1);
    }

    public List<Incripcion> findIncripcionEntities(int maxResults, int firstResult) {
        return findIncripcionEntities(false, maxResults, firstResult);
    }

    private List<Incripcion> findIncripcionEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Incripcion.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Incripcion findIncripcion(Long id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Incripcion.class, id);
        } finally {
            em.close();
        }
    }

    public int getIncripcionCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Incripcion> rt = cq.from(Incripcion.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
