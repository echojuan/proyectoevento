/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import modelo.Usuario;
import modelo.AsignaEspacio;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import modelo.Espacios;
import persistencia.exceptions.IllegalOrphanException;
import persistencia.exceptions.NonexistentEntityException;

/**
 *
 * @author Usuario
 */
public class EspaciosJpaController implements Serializable {

    public EspaciosJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Espacios espacios) {
        if (espacios.getAsignaEspacioList() == null) {
            espacios.setAsignaEspacioList(new ArrayList<AsignaEspacio>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario espAdmin = espacios.getEspAdmin();
            if (espAdmin != null) {
                espAdmin = em.getReference(espAdmin.getClass(), espAdmin.getUsuCedula());
                espacios.setEspAdmin(espAdmin);
            }
            List<AsignaEspacio> attachedAsignaEspacioList = new ArrayList<AsignaEspacio>();
            for (AsignaEspacio asignaEspacioListAsignaEspacioToAttach : espacios.getAsignaEspacioList()) {
                asignaEspacioListAsignaEspacioToAttach = em.getReference(asignaEspacioListAsignaEspacioToAttach.getClass(), asignaEspacioListAsignaEspacioToAttach.getEvEspId());
                attachedAsignaEspacioList.add(asignaEspacioListAsignaEspacioToAttach);
            }
            espacios.setAsignaEspacioList(attachedAsignaEspacioList);
            em.persist(espacios);
            if (espAdmin != null) {
                espAdmin.getEspaciosList().add(espacios);
                espAdmin = em.merge(espAdmin);
            }
            for (AsignaEspacio asignaEspacioListAsignaEspacio : espacios.getAsignaEspacioList()) {
                Espacios oldEvEspEspIdOfAsignaEspacioListAsignaEspacio = asignaEspacioListAsignaEspacio.getEvEspEspId();
                asignaEspacioListAsignaEspacio.setEvEspEspId(espacios);
                asignaEspacioListAsignaEspacio = em.merge(asignaEspacioListAsignaEspacio);
                if (oldEvEspEspIdOfAsignaEspacioListAsignaEspacio != null) {
                    oldEvEspEspIdOfAsignaEspacioListAsignaEspacio.getAsignaEspacioList().remove(asignaEspacioListAsignaEspacio);
                    oldEvEspEspIdOfAsignaEspacioListAsignaEspacio = em.merge(oldEvEspEspIdOfAsignaEspacioListAsignaEspacio);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Espacios espacios) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Espacios persistentEspacios = em.find(Espacios.class, espacios.getEspId());
            Usuario espAdminOld = persistentEspacios.getEspAdmin();
            Usuario espAdminNew = espacios.getEspAdmin();
            List<AsignaEspacio> asignaEspacioListOld = persistentEspacios.getAsignaEspacioList();
            List<AsignaEspacio> asignaEspacioListNew = espacios.getAsignaEspacioList();
            List<String> illegalOrphanMessages = null;
            for (AsignaEspacio asignaEspacioListOldAsignaEspacio : asignaEspacioListOld) {
                if (!asignaEspacioListNew.contains(asignaEspacioListOldAsignaEspacio)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain AsignaEspacio " + asignaEspacioListOldAsignaEspacio + " since its evEspEspId field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (espAdminNew != null) {
                espAdminNew = em.getReference(espAdminNew.getClass(), espAdminNew.getUsuCedula());
                espacios.setEspAdmin(espAdminNew);
            }
            List<AsignaEspacio> attachedAsignaEspacioListNew = new ArrayList<AsignaEspacio>();
            for (AsignaEspacio asignaEspacioListNewAsignaEspacioToAttach : asignaEspacioListNew) {
                asignaEspacioListNewAsignaEspacioToAttach = em.getReference(asignaEspacioListNewAsignaEspacioToAttach.getClass(), asignaEspacioListNewAsignaEspacioToAttach.getEvEspId());
                attachedAsignaEspacioListNew.add(asignaEspacioListNewAsignaEspacioToAttach);
            }
            asignaEspacioListNew = attachedAsignaEspacioListNew;
            espacios.setAsignaEspacioList(asignaEspacioListNew);
            espacios = em.merge(espacios);
            if (espAdminOld != null && !espAdminOld.equals(espAdminNew)) {
                espAdminOld.getEspaciosList().remove(espacios);
                espAdminOld = em.merge(espAdminOld);
            }
            if (espAdminNew != null && !espAdminNew.equals(espAdminOld)) {
                espAdminNew.getEspaciosList().add(espacios);
                espAdminNew = em.merge(espAdminNew);
            }
            for (AsignaEspacio asignaEspacioListNewAsignaEspacio : asignaEspacioListNew) {
                if (!asignaEspacioListOld.contains(asignaEspacioListNewAsignaEspacio)) {
                    Espacios oldEvEspEspIdOfAsignaEspacioListNewAsignaEspacio = asignaEspacioListNewAsignaEspacio.getEvEspEspId();
                    asignaEspacioListNewAsignaEspacio.setEvEspEspId(espacios);
                    asignaEspacioListNewAsignaEspacio = em.merge(asignaEspacioListNewAsignaEspacio);
                    if (oldEvEspEspIdOfAsignaEspacioListNewAsignaEspacio != null && !oldEvEspEspIdOfAsignaEspacioListNewAsignaEspacio.equals(espacios)) {
                        oldEvEspEspIdOfAsignaEspacioListNewAsignaEspacio.getAsignaEspacioList().remove(asignaEspacioListNewAsignaEspacio);
                        oldEvEspEspIdOfAsignaEspacioListNewAsignaEspacio = em.merge(oldEvEspEspIdOfAsignaEspacioListNewAsignaEspacio);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Short id = espacios.getEspId();
                if (findEspacios(id) == null) {
                    throw new NonexistentEntityException("The espacios with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Short id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Espacios espacios;
            try {
                espacios = em.getReference(Espacios.class, id);
                espacios.getEspId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The espacios with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<AsignaEspacio> asignaEspacioListOrphanCheck = espacios.getAsignaEspacioList();
            for (AsignaEspacio asignaEspacioListOrphanCheckAsignaEspacio : asignaEspacioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Espacios (" + espacios + ") cannot be destroyed since the AsignaEspacio " + asignaEspacioListOrphanCheckAsignaEspacio + " in its asignaEspacioList field has a non-nullable evEspEspId field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Usuario espAdmin = espacios.getEspAdmin();
            if (espAdmin != null) {
                espAdmin.getEspaciosList().remove(espacios);
                espAdmin = em.merge(espAdmin);
            }
            em.remove(espacios);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Espacios> findEspaciosEntities() {
        return findEspaciosEntities(true, -1, -1);
    }

    public List<Espacios> findEspaciosEntities(int maxResults, int firstResult) {
        return findEspaciosEntities(false, maxResults, firstResult);
    }

    private List<Espacios> findEspaciosEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Espacios.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Espacios findEspacios(Short id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Espacios.class, id);
        } finally {
            em.close();
        }
    }

    public int getEspaciosCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Espacios> rt = cq.from(Espacios.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
